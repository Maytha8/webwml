msgid ""
msgstr ""
"Project-Id-Version: partners.it\n"
"PO-Revision-Date: 2024-04-08 21:02+0200\n"
"Last-Translator: Luca Monducci <luca.mo@tiscali.it>\n"
"Language-Team: debian-l10n-italian <debian-l10n-italian@lists.debian.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/index.def:8
msgid "The Universal Operating System"
msgstr "Il Sistema Operativo Universale"

#: ../../english/index.def:12
msgid "DebConf is underway!"
msgstr "La DebConf è in corso!"

#: ../../english/index.def:15
msgid "DebConf Logo"
msgstr "Logo della DebConf"

#: ../../english/index.def:19
msgid "DC22 Group Photo"
msgstr "Foto di gruppo DC22"

#: ../../english/index.def:22
msgid "DebConf22 Group Photo"
msgstr "Foto di gruppo della DebConf22"

#: ../../english/index.def:26
msgid "DC23 Group Photo"
msgstr "Foto di gruppo DC23"

#: ../../english/index.def:29
msgid "DebConf23 Group Photo"
msgstr "Foto di gruppo della DebConf23"

#: ../../english/index.def:33
msgid "Debian Reunion Hamburg 2023"
msgstr "Raduno Debian a Amburgo 2023"

#: ../../english/index.def:36
msgid "Group photo of the Debian Reunion Hamburg 2023"
msgstr "Foto di gruppo del Raduno Debian a Amburgo 2023"

#: ../../english/index.def:40
msgid "MiniDebConf Brasília 2023"
msgstr "MiniDebConf Brasilia 2023"

#: ../../english/index.def:43
msgid "Group photo of the MiniDebConf Brasília 2023"
msgstr "Foto di gruppo della MiniDebConf a Brasilia 2023"

#: ../../english/index.def:47
msgid "Mini DebConf Regensburg 2021"
msgstr "Mini DebConf Ratisbona 2021"

#: ../../english/index.def:50
msgid "Group photo of the MiniDebConf in Regensburg 2021"
msgstr "Foto di gruppo della MiniDebConf a Ratisbona 2021"

#: ../../english/index.def:54
msgid "Screenshot Calamares Installer"
msgstr "Schermate dell'installatore Calamares"

#: ../../english/index.def:57
msgid "Screenshot from the Calamares installer"
msgstr "Schermate dell'installatore Calamares"

#: ../../english/index.def:61 ../../english/index.def:64
msgid "Debian is like a Swiss Army Knife"
msgstr "Debian è come un coltellino svizzero"

#: ../../english/index.def:68
msgid "People have fun with Debian"
msgstr "Persone che si divertono con Debian"

#: ../../english/index.def:71
msgid "Debian people at Debconf18 in Hsinchu really having fun"
msgstr "La gente di Debian alla Debconf18 di Hsinchu si sta davvero divertendo"

#: ../../english/template/debian/navbar.wml:31
msgid "Bits from Debian"
msgstr "Un po' di Debian"

#: ../../english/template/debian/navbar.wml:31
msgid "Blog"
msgstr "Blog"

#: ../../english/template/debian/navbar.wml:32
msgid "Micronews from Debian"
msgstr "Micronotizie da Debian"

#: ../../english/template/debian/navbar.wml:32
msgid "Micronews"
msgstr "Micronews"

#: ../../english/template/debian/navbar.wml:33
msgid "The Planet of Debian"
msgstr "Il Pianeta di Debian"

#: ../../english/template/debian/navbar.wml:33
msgid "Planet"
msgstr "Planet"

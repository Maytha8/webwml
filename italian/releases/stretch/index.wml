#use wml::debian::template title="Informazioni sul rilascio Debian &ldquo;stretch&rdquo;"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="9f9207921c4ea799d5d3daa8a0f4b0faedf2b073" maintainer="Luca Monducci"

<p>Debian <current_release_stretch> è stata rilasciata <a
href="$(HOME)/News/<current_release_newsurl_stretch/>"><current_release_date_stretch></a>.
<ifneq "9.0" "<current_release>"
  "Debian 9.0 è stata inizialmente rilasciata il <:=spokendate('2017-06-17'):>."
/>
Il rilascio includeva molte modifiche significative, descritte nel nostro
<a href="$(HOME)/News/2017/20170617">comunicato stampa</a> e nelle <a
href="releasenotes">Note di Rilascio</a>.</p>

<p><strong>Debian 9 è stata sostituita da
<a href="../buster/">Debian 10 (<q>buster</q>)</a>.
Gli aggiornamenti di sicurezza sono stati interrotti dal
<:=spokendate('2020-07-06'):>.</strong></p>

<p><strong>Stretch ha anche beneficiato del Supporto a Lungo Termine (LTS)
fino alla fine di giugno 2022. Il LTS era limitato a i386, amd64, armel,
armhf e arm64. Tutte le altre architetture non erano più supportate in
stretch. Per ulteriori informazioni, consultare la <a
href="https://wiki.debian.org/LTS">sezione LTS del Debian Wiki.</a>.
</strong></p>

<p>Per ottenere e installare Debian, consulta la pagina delle informazioni
sull'installazione e la Guida all'installazione. Per effettuare 
l'aggiornamento da una versione precedente di Debian, segui le istruzioni
nelle <a href="releasenotes">Note di Rilascio</a>.</p>

# Activate the following when LTS period starts.
<p>Architetture supportate durante il Supporto a Lungo Termine:</p>
<ul>
<li><a href="../../ports/amd64/">64-bit PC (amd64)</a>
<li><a href="../../ports/i386/">32-bit PC (i386)</a>
<li><a href="../../ports/armel/">EABI ARM (armel)</a>
<li><a href="../../ports/armhf/">Hard Float ABI ARM (armhf)</a>
<li><a href="../../ports/arm64/">64-bit ARM (AArch64)</a>
</ul>

<p>Architetture supportate al rilascio iniziale di stretch:</p>
<ul>
<li><a href="../../ports/amd64/">64-bit PC (amd64)</a>
<li><a href="../../ports/i386/">32-bit PC (i386)</a>
<li><a href="../../ports/armel/">EABI ARM (armel)</a>
<li><a href="../../ports/armhf/">Hard Float ABI ARM (armhf)</a>
<li><a href="../../ports/mipsel/">MIPS (little endian)</a>
<li><a href="../../ports/mips/">MIPS (big endian)</a>
<li><a href="../../ports/s390x/">IBM System z</a>
<li><a href="../../ports/arm64/">64-bit ARM (AArch64)</a>
<li><a href="../../ports/ppc64el/">POWER Processors</a>
<li><a href="../../ports/mips64el/">64-bit MIPS (little endian)</a>
</ul>

<p>Contrariamente a quanto ci auguriamo, è possibile che esistano alcuni
problemi, nonostante sia dichiarata <em>stabile</em>. Abbiamo compilato <a
href="errata">una lista dei principali problemi noti</a>, e potete sempre
segnalarci altri problemi.</p>

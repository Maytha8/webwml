#use wml::debian::template title="Информация по безопасности" GEN_TIME="yes" MAINPAGE="true"
#use wml::debian::toc
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="452896bf2af61f852728d258a34c0609dd632373"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
<li><a href="#keeping-secure">Как поддерживать безопасность вашей системы Debian</a></li>
<li><a href="#DSAS">Последние рекомендации по безопасности</a></li>
<li><a href="#infos">Источники информации по безопасности</a></li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span>Debian очень серьёзно относится к безопасности. Мы обрабатываем все проблемы безопасности, доведённые да нашего сведения, и гарантируем, что они будут устранены в разумные сроки.</p>
</aside>

<p>
Опыт показывает, что принцип <q>безопасность через неизвестность</q> не работает. Поэтому публичное раскрытие информации позволяет быстрее и лучше решать проблемы безопасности. В связи с этим, на этой странице рассматривается статус Debian в отношении различных известных проблем в безопасности, которые потенциально могут повлиять на операционную систему Debian.
</p>

<p>
Проект Debian координирует многие рекомендации по безопасности с другими производителями свободного ПО, в результате эти рекомендации публикуются в тот же день, когда уязвимость становится достоянием общественности.
Чтобы получать последние рекомендации Debian по безопасности, подпишитесь на список рассылки <a href="https://lists.debian.org/debian-security-announce/">debian-security-announce</a>.
</p>

<p>
Кроме того, Debian принимает участие в стандартизации в области безопасности:
</p>

<ul>
  <li><a href="#DSAS">Рекомендации по безопасности Debian</a> являются <a href="cve-compatibility">CVE-совместимыми</a> (смотрите <a href="crossreferences">перекрёстные ссылки</a>).</li>
  <li>Debian <a href="oval/">публикует</a> информацию о безопасности с помощью <a href="https://github.com/CISecurity/OVALRepo">открытого языка оценки уязвимостей (Open Vulnerability Assessment Language, OVAL)</a></li>
</ul>

<h2><a id="keeping-secure">Как поддерживать безопасность вашей системы Debian</a></h2>



<p>

Можно установить пакет <a href="https://packages.debian.org/stable/admin/unattended-upgrades">unattended-upgrades</a>, чтобы
автоматически устанавливать самые свежие обновления безопасности (и другие обновления) на вашу систему.

На соответствующей <a href="https://wiki.debian.org/UnattendedUpgrades">вики-странице</a>
имеется подробная информация о том, как вручную настроить <tt>unattended-upgrades</tt>.

<p>
Более подробную информацию о вопросах безопасности в Debian см. в ЧАВО и нашей документации:
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="faq">ЧАВО по безопасности</a></button> <button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="../doc/user-manuals#securing">Обеспечение безопасности Debian</a></button></p>


<aside class="light">
  <span class="fa fa-rss fa-5x"></span>
</aside>

<h2><a id="DSAS">Последние рекомендации по безопасности</a> <a class="rss_logo" style="float: none;" href="dsa">RSS</a></h2>

<p>Ниже приводятся последние рекомендации по безопасности Debian (DSA), отправленные в список рассылки <a href="https://lists.debian.org/debian-security-announce/">debian-security-announce</a>.
<br><b>T</b> представляет собой ссылку на информацию из <a href="https://security-tracker.debian.org/tracker">системы отслеживания безопасности Debian</a>, номера DSA являются ссылками на сообщение в списке рассылки.

<p>
#include "$(ENGLISHDIR)/security/dsa.list"
</p>

{#rss#:
<link rel="alternate" type="application/rss+xml"
 title="Рекомендации по безопасности Debian (только заголовки)" href="dsa">
<link rel="alternate" type="application/rss+xml"
 title="Рекомендации по безопасности Debian (резюме)" href="dsa-long">
:#rss#}

<h2><a id="infos">Источники информации по безопасности</a></h2>
#include "security-sources.inc"

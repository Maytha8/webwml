#use wml::debian::translation-check translation="7ce619292bb7825470d6cb611887d68f405623ff" maintainer="Lev Lamberov"
<define-tag pagetitle>Выпущен Debian 11 <q>bullseye</q></define-tag>
<define-tag release_date>2021-08-14</define-tag>
#use wml::debian::news


<p>После 2 лет, 1 месяца и 9 дней разработки Проект Debian с гордостью
представляет новую стабильную версию 11 (кодовое имя <q>bullseye</q>),
которая будет поддерживаться в течение следующих 5 лет благодаря совместной
работе <a href="https://security-team.debian.org/">команды безопасности Debian</a>
и команды <a href="https://wiki.debian.org/LTS">долгосрочной поддержки Debian</a>.
</p>

<p>
Debian 11 <q>bullseye</q> поставляется с несколькими окружениями и приложениями
рабочего стола. Помимо прочего, выпуск включает в себя следующие окружения рабочего стола:
</p>
<ul>
<li>Gnome 3.38,</li>
<li>KDE Plasma 5.20,</li>
<li>LXDE 11,</li>
<li>LXQt 0.16,</li>
<li>MATE 1.24,</li>
<li>Xfce 4.16.</li>
</ul>


<p>Этот выпуск содержит более 11,294 новых пакета, а общее число пакетов составляет 59,551.
Кроме того, более 9,519 пакетов были отмечены как <q>устаревшие</q>
и удалены. 42,821 пакет обновлён, а 5,434 пакета остались без
изменений.
</p>

<p>
<q>bullseye</q> становится нашим первым выпуском, в котором ядро Linux поддерживает
файловую систему exFAT и по умолчанию использует её для монтирования файловых систем exFAT.
Поэтому более не требуется использовать файловую систему в пространстве пользователя,
предоставляемую пакетом exfat-fuse. Инструменты для создания и проверки
файловой системы exFAT предоставляются в пакете exfatprogs.
</p>


<p>
Большая часть современных принтеров можно использовать бездрайверную печать и сканирование
без необходимости устанавливать драйверы конкретного поставщика (часто несвободные).

<q>bullseye</q> содержит новый пакет, ipp-usb, который использует нейтральный
протокол IPP-через-USB, поддерживаемый многими современными принтерами. Это позволяет
USB-устройству считаться сетевым устройством. Официальный бездрайверный движок SANE
предоставляется sane-escl в пакете libsane1. Для него используется протокол eSCL.
</p>

<p>
Systemd в <q>bullseye</q> по умолчанию включает опцию постоянного журнала
с внутренней альтернативой для использования энергозависимого запоминающего устройства.
Это позволяет пользователям, не использующим специальные возможности, удалить
традиционные службы журналирования и использовать один только журнал systemd.
</p>

<p>
Команда Debian Med участвовала в борьбе с COVID-19, создавая пакеты ПО
для исследования вируса на уровне генетических последовательностей
и для борьбы с пандемией с помощью инструментов, используемых в эпидемиологии. Эта работа
будет продолжена, а основное внимание будет уделено инструментам машинного обучения для
обеих областей. Работа команды над контролем качества и непрерывной интеграцией
является критической для единообразных воспроизводимых результатов, необходимых в этих науках.

Смесь Debian Med поддерживает ряд критичных приложений, которые сейчас
выигрывают от SIMD Everywhere. Для установки пакетов, сопровождаемых командой Debian Med,
установите метапакеты с именами вида med-*, сейчас они имеют версию 3.6.x.
</p>

<p>
Для китайского, японского, корейского и многих других языков теперь используется метод
ввода Fcitx 5, который является развитием популярного Fcitx4, используемого в выпуске <q>buster</q>.
Эта новая версия намного лучше поддерживает дополнения Wayland (дисплейного менеджера по умолчанию).
</p>

<p>
Debian 11 <q>bullseye</q> включает в себя множество обновлённых пакетов ПО (более
72% всех пакетов из прошлого выпуска). Это такие пакеты как
</p>
<ul>
<li>Apache 2.4.48</li>
<li>BIND DNS Server 9.16</li>
<li>Calligra 3.2</li>
<li>Cryptsetup 2.3</li>
<li>Emacs 27.1</li>
<li>GIMP 2.10.22</li>
<li>GNU Compiler Collection 10.2</li>
<li>GnuPG 2.2.20</li>
<li>Inkscape 1.0.2</li>
<li>LibreOffice 7.0</li>
<li>Linux kernel 5.10 series</li>
<li>MariaDB 10.5</li>
<li>OpenSSH 8.4p1</li>
<li>Perl 5.32</li>
<li>PHP 7.4</li>
<li>PostgreSQL 13</li>
<li>Python 3, 3.9.1</li>
<li>Rustc 1.48</li>
<li>Samba 4.13</li>
<li>Vim 8.2</li>
<li>более 59,000 других готовых к использованию пакетов ПО, собранных из
более чем 30,000 пакетов с исходным кодом.</li>
</ul>

<p>
Предлагая такой широкий выбор пакетов и, как обычно, большое число поддерживаемых
архитектур, Debian по-прежнему остаётся верен свой цели быть <q>универсальной
операционной системой</q>. Он подходит для множества различных вариантов использования:
для настольных систем и нетбуков; для серверов разработки и кластерных систем; для баз данных,
веб и серверов хранения. В то же время дополнительные усилия, уделяемые
качеству, например автоматизированное тестирование установки и обновления для
всех пакетов архива Debian, позволяют гарантировать, что <q>bullseye</q> оправдывает
те высокие ожидания, которые пользователи возлагают на стабильный
выпуск Debian.
</p>

<p>
Всего поддерживается девять архитектур:
64-битные ПК / Intel EM64T / x86-64 (<code>amd64</code>),
32-битные ПК / Intel IA-32 (<code>i386</code>),
64-битные Motorola/IBM PowerPC с порядком байтов от младшего с старшему (<code>ppc64el</code>),
64-битные IBM S/390 (<code>s390x</code>),
для ARM, <code>armel</code>
и <code>armhf</code> для более старого и более нового 32-битного оборудования,
а также <code>arm64</code> для 64-битной архитектуры <q>AArch64</q>,
также и для MIPS поддерживаются архитектуры <code>mipsel</code> (с порядком байтов
от младшего к старшему) для 32-битного оборудования и архитектура
<code>mips64el</code> для 64-битного оборудования с порядком байтов от младшего к старшему.
</p>

<h3>Хотите попробовать?</h3>
<p>
Если вы хотите просто попробовать Debian 11 <q>bullseye</q> без установки,
то можете использовать один из доступных <a href="$(HOME)/CD/live/">живых образов</a>, которые загружают
полноценную операционную систему в состоянии только для чтения в память компьютера.
</p>

<p>
Эти живые образы предоставляются для архитектур <code>amd64</code> и
<code>i386</code> и доступны для DVD, USB-носителей и
сетевой загрузки. Пользователь может сделать выбор из нескольких окружений рабочего
стола: GNOME, KDE Plasma, LXDE, LXQt, MATE, Xfce и Xfce.
Debian Live <q>bullseye</q> является стандартным живым образом, поэтому можно запустить
базовую систему Debian без какого-либо графического окружения пользователя.
</p>

<p>
Если вам понравится наша операционная система, вы сможете выполнить установку
прямо с живого образа на жёсткий диск вашего компьютера. Живой образ включает
в себя независимую программу установки Calamares, а также стандартную программу установки Debian.
Дополнительная информация доступна в
<a href="$(HOME)/releases/bullseye/releasenotes">информации о выпуске</a> и разделе
<a href="$(HOME)/CD/live/">живых установочных образов</a>
веб-сайта Debian.
</p>

<p>
Если вы решите сразу установить Debian 11 <q>bullseye</q> на жёсткий
диск вашего компьютера, то вы можете сделать выбор из множества установочных носителей,
таких как диски Blu-ray, DVD, компакт-диски, USB-носители или через сеть.
При установке с этих образов можно выбрать несколько окружений рабочего
стола: Cinnamon, GNOME, KDE Plasma Desktop and Applications, LXDE, LXQt, MATE, а также
Xfce.
Кроме того, доступны <q>мультиархитектурные</q> компакт-диски, которые поддерживают
установку нескольких архитектур с одного диска. Также вы всегда можете
создать загрузочный USB-носитель (см. подробности в
<a href="$(HOME)/releases/bullseye/installmanual">руководстве по установке</a>).
</p>

# Translators: some text taken from /devel/debian-installer/News/2021/20210802

<p>
Была проведена большая работа над программой установки Debian,
которая привела к улучшенной поддержке оборудования и другим возможностям.
</p>
<p>
В некоторых случаях успешная установка всё ещё может приводить к проблемам с дисплеем
при перезагрузке в установленную систему; для этих случаев имеются
<a href="https://www.debian.org/releases/bullseye/amd64/ch06s04#completing-installed-system">несколько временных решений</a>,
которые могут помочь войти в систему.
Кроме того есть
<a href="https://www.debian.org/releases/bullseye/amd64/ch06s04#completing-installed-system">процедура на основе isenkram</a>,
позволяющая пользователям обнаружить и исправить отсутствие микропрограмм в их системах
автоматическим способом. Конечно, следует взвесить все за и против
использования этого инструмента, поскольку скорее всего будет нужно установить
несвободные пакеты.</p>

<p>
  Кроме того, были улучшены
  <a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/">несвободные установочные образы, содержащие пакеты с микропрограммами</a>,
  теперь они могут предвосхитить необходимость микропрограмм
  для установленной системы (например, микропрограмм для графических карт AMD или Nvidia, а также
  для более новых поколений аудиокарт Intel).
</p>

<p>
Для пользователей облачных сервисов Debian предлагает поддержку множества
широко известных облачных платформ. Официальные образы Debian можно выбрать
в соответствующих магазинах образов. Кроме того, Debian публикует <a
href="https://cloud.debian.org/images/openstack/current/">заранее подготовленные
образы OpenStack</a> для архитектур <code>amd64</code> и <code>arm64</code>,
которые готовы к загрузке и использованию в локальных облачных системах.
</p>

<p>
Теперь Debian может быть установлен на 76 языках, большинство из них доступны
и в текстовом, и в графическом пользовательских окружениях.
</p>

<p>
Установочные образы можно загрузить прямо сейчас через
<a href="$(HOME)/CD/torrent-cd/">bittorrent</a> (рекомендуемый метод),
<a href="$(HOME)/CD/jigdo-cd/#which">jigdo</a> или
<a href="$(HOME)/CD/http-ftp/">HTTP</a>; дополнительную информацию см. в разделе
<a href="$(HOME)/CD/">Debian на компакт-дисках</a>. Также <q>bullseye</q> скоро
будет доступен на физических DVD, CD-ROM и дисках Blu-ray от
множества <a href="$(HOME)/CD/vendors">поставщиков</a>.
</p>


<h3>Обновление Debian</h3>
<p>
Обновление до Debian 11 с предыдущего выпуска, Debian 10
(кодовое имя <q>buster</q>), для большинства вариантов настройки
осуществляется автоматически с помощью APT.
</p>

<p>
Для выпуска bullseye компонент с обновлениями безопасности теперь называется bullseye-security,
пользователям следует изменить свои файлы источников APT соответствующим образом в ходе обновления.
Если в ваших настройках APT используются закрепление или <code>APT::Default-Release</code>,
то для этих параметров тоже скорее всего потребуются изменения. Дополнительную информацию смотрите в разделе
<a href="https://www.debian.org/releases/bullseye/amd64/release-notes/ch-information#security-archive">Изменена структура архива безопасности</a>
в информации о выпуске.
</p>

<p>
Если вы выполняется обновление удалённым образом, то ознакомьтесь с разделом
<a href="$(HOME)/releases/bullseye/amd64/release-notes/ch-information#ssh-not-available">Во время обновления невозможны новые SSH-соединения</a>.
</p>

<p>
Как и всегда, системы Debian можно обновить без особых усилий, на месте,
без вынужденного простоя. Тем не менее настоятельно рекомендуется прочесть
<a href="$(HOME)/releases/bullseye/releasenotes">информацию о выпуске</a>, а
также <a href="$(HOME)/releases/bullseye/installmanual">руководство по
установке</a>, в которых описаны возможные проблемы и подробные инструкции по
выполнению установки и обновления. В ближайшие недели будет обновляться информация о
выпуске, а также появятся переводы на дополнительные языки.
</p>


<h2>О Debian</h2>

<p>
Debian &mdash; свободная операционная система, разрабатываемая
тысячами добровольцев со всего мира, сотрудничающих через Интернет.
Сильными сторонами проекта Debian являются его добровольцы, приверженность
Общественному договору Debian и Свободному ПО, а также его обязательства
предоставить настолько хорошую операционную систему, насколько это возможно. Данный
новый выпуск является ещё одним важным шагом в этом направлении.
</p>


<h2>Контактная информация</h2>

<p>
Более подробную информацию вы можете получить на веб-сайте Debian
<a href="$(HOME)/">https://www.debian.org/</a> либо отправив письмо по адресу
&lt;press@debian.org&gt;.
</p>

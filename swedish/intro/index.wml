#use wml::debian::template title="Introduktion till Debian" MAINPAGE="true" FOOTERMAP="true"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="1cf898ff877f907fd4f3471e33d7d506701907f3"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div class="row">

  <div class="column column-left" id="community">
    <div style="text-align: center">
      <span class="fa fa-users fa-3x" style="float:left;margin-right:5px;margin-bottom:5px;"></span>
      <a id="community"></a>
      <h2>Debian är en gemenskap</h2>
      <p>Tusentals frivilliga runt hela världen arbetar tillsammans på
      operativsystemet Debian, och prioriterar fri mjukvara och mjukvara med
      öppen källkod. Möt Debianprojektet.</p>
    </div>

    <div style="text-align: left">
      <ul>
        <li>
          <a href="people">Folk:</a>
          Vilka vi är och vad vi gör
        </li>
        <li>
          <a href="philosophy">Filosofi:</a>
          Varför vi gör det och hur vi gör det
        </li>
        <li>
          <a href="../devel/join/">Engagera dig:</a>
          Hur du blir en bidragslämnare för Debian
        </li>
        <li>
          <a href="help">Bidra:</a>
          Hur du kan hjälpa Debian
        </li>
        <li>
          <a href="../social_contract">Socialt Kontrakt:</a>
          Vår moraliska agenda
        </li>
        <li>
          <a href="diversity">Alla är välkomna:</a>
          Mångfaldserkännande
        </li>
        <li>
          <a href="../code_of_conduct">För deltagare:</a>
          Debians uppförandekod
        </li>
        <li>
          <a href="../partners/">Partners:</a>
          Företag och organisationer som tillhandahåller pågående assistans
          till Debianprojektet
        </li>
        <li>
          <a href="../donations">Donationer</a>
          Hur kan man sponsra Debianprojektet
        </li>
        <li>
          <a href="../legal/">Rättsliga frågor:</a>
          Licenser, varumärken, integritetspolicy, patentpolicy, osv.
        </li>
        <li>
          <a href="../contact">Kontakt:</a>
          Hur man kan kontakta oss
        </li>
      </ul>
   </div>
</div>


  <!-- right column -->
  <div class="column column-right" id="software">
    <div style="text-align: center">
      <span class="fa fa-desktop fa-3x" style="float:left;margin-right:5px;margin-bottom:5px;"></span>
      <a id="software"></a>
      <h2>Debian är ett operativsystem</h2>
      <p>Debian är ett fritt operativsystem, utvecklat och underhållet av
      Debianprojektet. En fri linuxdistribution med tusentals program för att
      möta våra användares behov.</p>
    </div>

    <div style="text-align: left">
   <ul>
     <li>
       <a href="../distrib">Hämta:</a>
       Flera varianter av Debianavbildningar
     </li>
     <li>
       <a href="why_debian">Varför Debian</a>
       Anledningar till att välja Debian
     </li>
     <li>
       <a href="../support">Support:</a>
       Var du hittar hjälp
     </li>
     <li>
       <a href="../security">Säkerhet:</a>
       Aktuella bulletiner och källor till säkerhetsinformation
     </li>
     <li>
       <a href="../distrib/packages">Mjukvara:</a>
       Sök och bläddra i den långa listan på vår mjukvara
     </li>
     <li>
       <a href="../doc">Dokumentation</a>
       Installationsguide, FAQ, HOWTOs, Wiki och mer
     </li>
     <li>
       <a href="../Bugs"> Felrapporteringssystemet:</a>
       Hur du rapporterar ett fel, BTS-dokumentation
     </li>
     <li>
       <a href="https://lists.debian.org/">Sändlistor</a>
       Samling av Debianlistor för användare, utvecklare, osv.
     </li>
     <li>
       <a href="../blends">Pure Blends:</a>
       Metapaket för speciella behov
     </li>
     <li>
       <a href="../devel">Utvecklarhörnet:</a>
       Information primärt av intresse för Debianutvecklare
     </li>
     <li>
       <a href="../ports">Anpassningar/Arkitekturer:</a>
       Debianstöd för olika CPU-arkitekturer
     </li>
     <li>
       <a href="search">Sök:</a>
       Information om hur du använder Debians sökmotor
     </li>
     <li>
       <a href="cn">Språk:</a>
       Språkinställningar för Debians webbplats
     </li>
   </ul>
 </div>
  </div>

</div>

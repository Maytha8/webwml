#use wml::debian::template title="Debian &ldquo;Bookworm&rdquo; Installationsinformation" NOHEADER="true"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/bookworm/release.data"
#use wml::debian::translation-check translation="ab3c0fa63d12dbcc8e7c3eaf4a72beb7b56d9741"

<h1>Installera Debian <current_release_bookworm></h1>

<if-stable-release release="trixie">
<p><strong>Debian 12 har efterträtts av
<a href="../../trixie/">Debian 13 (<q>Trixie</q>)</a>. Vissa av dessa
installationsavbildningarna kanske inte längre är tillgängliga, eller
fungerar inte, och du rekommenderas därför att installera trixie istället.
</strong></p>
</if-stable-release>


<p>
<strong>För att installera Debian </strong> <current_release_bookworm>
(<em>Bookworm</em>), hämta någon av dessa avbildningar (alla i386- och amd64-
CD/DVD-avbildningar kan även användas på USB-minnen):
</p>

<div class="line">
<div class="item col50">
	<p><strong>netinst CD-avbildning</strong></p>
		<netinst-images />
</div>


</div>

<div class="line">
<div class="item col50">
	<p><strong>kompletta CD-uppsättningar</strong></p>
		<full-cd-images />
</div>

<div class="item col50 lastcol">
	<p><strong>kompletta DVD-uppsättningar</strong></p>
		<full-dvd-images />
</div>

</div>

<div class="line">
<div class="item col50">
<p><strong>CD (via <a href="$(HOME)/CD/torrent-cd">BitTorrent</a>)</strong></p>
<full-cd-torrent />
</div>

<div class="item col50 lastcol">
<p><strong>DVD (via <a href="$(HOME)/CD/torrent-cd">BitTorrent</a>)</strong></p>
<full-dvd-torrent />
</div>

</div>

<div class="line">
<div class="item col50">
<p><strong>CD (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong></p>
<full-cd-jigdo />
</div>

<div class="item col50 lastcol">
<p><strong>DVD (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong></p>
<full-dvd-jigdo />
</div>


</div>

<div class="line">
<div class="item col50">
<p><strong>Blu-ray  (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong></p>
<full-bluray-jigdo />
</div>

<div class="item col50 lastcol">
<p><strong>andra avbildningar (netboot, flexibelt usb-minne, osv.)</strong></p>
<other-images />
</div>
</div>



<p>
<strong>Kommentarer</strong>
</p>
<ul>
   <li>
   För att hämta kompletta CD- och DVD-avbildningar rekommenderas BitTorrent
   eller jigdo.
   </li><li>
   För de mindre vanliga arkitekturerna så finns det bara ett begränsat antal av CD- och DVD-uppsättningarna tillgängligt som ISO-avbildningar eller via BitTorrent. De fullständiga uppsättningarna finns tillgängliga via jigdo. 
   </li><li>
   För installationsavbildningarna finns verifikationsfiler (<tt>SHA256SUMS</tt>,
	<tt>SHA512SUMS</tt> och andra) tillgängliga i samma katalog som
   avbildningarna.
   </li>
</ul>


<h1>Dokumentation</h1>

<p>
<strong>Om du endast kommer att läsa ett dokument</strong> innan du installerar,
läs <a href="../i386/apa">installationshjälp</a>, en snabbgenomgång av
installationsprocessen. Annan användbar dokumentation inkluderar:
</p>

<ul>
<li><a href="../installmanual">Bookworm installationsguide</a><br />
detaljerade installationsinstruktioner</li>
<li><a href="https://wiki.debian.org/DebianInstaller/FAQ">Debian-Installer FAQ</a>
och <a href="$(HOME)/CD/faq/">Debian-CD FAQ</a><br />
vanliga frågor och svar</li>
<li><a href="https://wiki.debian.org/DebianInstaller">Debian-Installer Wiki</a><br />
gemenskapsunderhållen dokumentation</li>
</ul>

<h1 id="errata">Kända problem</h1>

<p>
Detta är en listas på kända problem i installeraren som skeppas med
Debian <current_release_bookworm>. Om du har haft problem med att installera
Debian och inte kan se ditt problem i listan här, vänligen skicka oss en
<a href="$(HOME)/releases/stable/amd64/ch05s04#submit-bug">installationsrapport</a>
(på engelska) som beskriver problemet eller 
<a href="https://wiki.debian.org/DebianInstaller/BrokenThings">kontrollera wikin</a>
efter andra kända problem.
</p>

## Translators: copy/paste from devel/debian-installer/errata
<h3 id="errata-r0">Kända problem i utgåva 12.0</h3>

<dl class="gloss">

<!--
     <dt>Desktop installations may not work using CD#1 alone</dt>

     <dd>Due to space constraints on the first CD, not all of the
     expected GNOME desktop packages fit on CD#1. For a successful
     installation, use extra package sources (e.g. a second CD or a
     network mirror) or use a DVD instead.

     <br /> <b>Status:</b> It is unlikely more efforts can be made to
     fit more packages on CD#1. </dd>
-->
</dl>

<p>
Förbättrade versioner av installationssystemet utvecklas för nästa
Debianutgåva, och kan också användas för att installera Bookworm.
För detaljer, se
<a href="$(HOME)/devel/debian-installer/">sidan för projektet Debian-Installer</a>.
</p>

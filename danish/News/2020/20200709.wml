# Status: [open-for-edit]
# $Rev$
#use wml::debian::translation-check translation="9c0ebe940eaf29e78367427aea8e02f46fb70bcd"
<define-tag pagetitle>Debian 8 Long Term Support ophører</define-tag>
<define-tag release_date>2020-07-09</define-tag>
#use wml::debian::news

<p>Debians hold bag Long Term Support (LTS) annoncerer hermed at understøttelse 
af Debian 8 <q>jessie</q> er ophørt pr. 30. juni 2020, fem år efter den første 
udgave på den 26. april 2015.</p>

<p>Debian vil ikke længere levere sikkerhedsopdateringer til Debian 8.  En 
delmængde af pakkerne i <q>jessie</q> vil blive understøttet af eksterne 
grupper.  Uddybende oplysninger finder man i 
<a href="https://wiki.debian.org/LTS/Extended">Extended LTS</a>.</p>

<p>LTS-holdet forbereder sig på overgangen til Debian 9 <q>stretch</q>, som er 
den aktuelle gamle stabile (oldstable) udgave.  Den 6. juli 2020 overtog 
LTS-holdet understøttelsen fra Sikkerhedsholdet, mens den sidste punktopdatering 
af <q>stretch</q> vil blive udgivet den 18. juli 2020.</p>

<p>Debian 9 vil også modtage Long Term Support i fem år efter den første udgave, 
dvs. understøttelsen ophører den 30. juni 2022.  De understøttede arkitekturer 
er fortsat amd64, i386, armel og armhf.  Desuden er vi glade for at kunne 
offentliggøre, at understøttelsen for første gang udvides til arkitekturen 
arm64.</p>

<p>For flere oplysninger om anvendelse af <q>stretch</q> LTS og opgradering fra 
<q>jessie</q> LTS, se <a href="https://wiki.debian.org/LTS/Using">\
LTS/Using</a>.</p>

<p>Debian og dets LTS-hold takker hermed alle brugere, udviklere og sponsorer, 
som har bidraget til at gøre det muligt, at give tidligere stabile udgaver 
levetidsforlængelser, og som har gjort denne LTS til en succes.</p>

<p>Hvis du er afhængig af Debian LTS, så overvej at 
<a href="https://wiki.debian.org/LTS/Development">deltage i holdet</a>, 
levere patches, teste eller 
<a href="https://wiki.debian.org/LTS/Funding">give økonomiske bidrag til 
arbejdet</a>.</p>


<h2>Om Debian</h2>

<p>Debian-projektet blev grundlagt i 1993 af Ian Murdock, som et helt frit
fællesskabsprojekt.  Siden den gang, er projektet vokset til at være et af de
største og mest indflydelsesrige open source-projekter.  Tusindvis af
frivillige fra hele verden samarbejder om at fremstille og vedligeholde
Debian-software.  Med oversættelser til 70 sprog, og med understøttelse af et
enormt antal computertyper, kalder Debian sig det <q>universelle
styresystem</q>.</p>

<h2>Flere oplysninger</h2>
<p>Flere oplysninger om Debian Long Term Support finder man på 
<a href="https://wiki.debian.org/LTS/">https://wiki.debian.org/LTS/</a>.</p>


<h2>Kontaktoplysninger</h2>

<p>For flere oplysninger, besøg Debians websider på 
<a href="$(HOME)/">https://www.debian.org/</a> eller send e-mail på engelsk til
&lt;press@debian.org&gt;.</p>

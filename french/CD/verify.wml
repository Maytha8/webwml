#use wml::debian::translation-check translation="942f98b47dc32183d181b4c309c038a3e8c33f3d" maintainer="Jean-Pierre Giraud"
#use wml::debian::cdimage title="Vérification de l'authenticité des images Debian" BARETITLE=true

# Translators :
# David Prévot, 2011.
# Jean-Pierre Giraud, 2015, 2022.
# Jean-Paul Guillonneau, 2016.

<p>
Les publications officielles des images d'installation et des images autonomes de
Debian sont fournies avec des fichiers de sommes de contrôle signés. Vous
pouvez les rechercher à coté des images dans les répertoires
<code>iso-cd</code>, <code>jigdo-dvd</code>, <code>iso-hybrid</code>, etc. Ces
fichiers vous permettent de vérifier que les images téléchargées sont
correctes. Tout d'abord, les sommes de contrôle permettent de vérifier que les
images n'ont pas été corrompues pendant leur téléchargement. Ensuite, les
signatures des fichiers de sommes de contrôle permettent de confirmer que les
images sont celles créées par Debian et qu’elles n'ont pas été altérées.
</p>

<p>
Pour valider le contenu d'un fichier d'image, assurez-vous d'utiliser l'outil
de sommes de contrôle approprié. Des algorithmes de sommes de contrôle sûrs
d'un point de vue cryptographique (SHA256 et SHA512) sont utilisés pour toutes
les publications. Pour leur mise en œuvre, les outils correspondants
<code>sha256sum</code> ou <code>sha512sum</code> sont à employer.
</p>

<p>
Pour s'assurer que les fichiers de sommes de contrôle sont eux-mêmes
corrects, utilisez une implémentation de OpenPGP (telle que GnuPG, Sequoia-PGP,
PGPainless ou GopenPGP) pour les vérifier à l'aide des fichiers de
signatures qui les accompagnent (par exemple <code>SHA512SUMS.sign</code>). Les
clefs utilisées sont toutes dans le <a href="https://keyring.debian.org">\
trousseau GPG Debian</a> et la meilleure façon de les vérifier est d'utiliser
ce trousseau pour les valider à l'aide du réseau de confiance. Pour faciliter
la vie des personnes qui n'ont pas accès à une machine Debian, voici les détails
des clefs qui ont été utilisées pour signer les publications de ces dernières
années et les liens pour télécharger directement les clés publiques :
</p>

#include "$(ENGLISHDIR)/CD/CD-keys.data"

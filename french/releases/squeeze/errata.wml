#use wml::debian::template title="Debian 6.0 &mdash; Errata" BARETITLE=true
#use wml::debian::toc
#use wml::debian::translation-check translation="476ef1924df8fdb75ad78952645435d512788254" maintainer="David Prévot"

#include "$(ENGLISHDIR)/releases/info"

<toc-display/>


# <toc-add-entry name="known_probs">Problèmes connus</toc-add-entry>
<toc-add-entry name="security">Problèmes de sécurité</toc-add-entry>

<p>Debian 6.0 <q>Squeeze</q> a atteint la fin de son cycle de prise en charge de sécurité.
Cependant, l'équipe <a href="https://wiki.debian.org/LTS">Squeeze LTS</a>
(<q>Long Time Support</q>, prise en charge sur le long terme) 
continue de produire une prise en charge de sécurité pour cette version.</p>

<p>Veuillez noter que ces mises à jour ne sont pas distribuées par les miroirs de
sécurité habituels ; à la place vous devez ajouter le dépôt <q>squeeze-lts</q> pour
les utiliser.</p>

<p>
Si vous utilisez APT, ajoutez la ligne suivante à votre fichier
<tt>/etc/apt/sources.list</tt> pour récupérer les dernières mises à jour sur la
sécurité&nbsp;:
</p>

<pre>
  deb http://http.debian.net/debian/ squeeze-lts main contrib non-free
</pre>

<p>
Ensuite, lancez <kbd>apt-get update</kbd> suivi de <kbd>apt-get upgrade</kbd>.
</p>


<toc-add-entry name="pointrelease">Les versions intermédiaires</toc-add-entry>

<p>
Parfois, dans le cas où il y a plusieurs problèmes critiques ou plusieurs mises
à jour de sécurité, la version de la distribution est mise à jour.
Généralement, ces mises à jour sont indiquées comme étant des versions
intermédiaires.</p>

<ul>
  <li>La première version intermédiaire, 6.0.1, a été publiée le
      <a href="$(HOME)/News/2011/20110319">19 mars 2011</a>.</li>
  <li>La deuxième version intermédiaire, 6.0.2, a été publiée le
      <a href="$(HOME)/News/2011/20110625">25 juin 2011</a>.</li>
  <li>La troisième version intermédiaire, 6.0.3, a été publiée le
      <a href="$(HOME)/News/2011/20111008">8 octobre 2011</a>.</li>
  <li>La quatrième version intermédiaire, 6.0.4, a été publiée le
      <a href="$(HOME)/News/2012/20120128">28 janvier 2012</a>.</li>
  <li>La cinquième version intermédiaire, 6.0.5, a été publiée le
      <a href="$(HOME)/News/2012/20120512">12 mai 2012</a>.</li>
  <li>La sixième version intermédiaire, 6.0.6, a été publiée le
      <a href="$(HOME)/News/2012/20120929">29 septembre 2012</a>.</li>
  <li>La septième version intermédiaire, 6.0.7, a été publiée le
      <a href="$(HOME)/News/2013/20130223">23 février 2013</a>.</li>
  <li>La huitième version intermédiaire, 6.0.8, a été publiée le
      <a href="$(HOME)/News/2013/20131020">20 octobre 2013</a>.</li>
  <li>La neuvième version intermédiaire, 6.0.9, a été publiée le
      <a href="$(HOME)/News/2014/20140215">15 février 2014</a>.</li>
  <li>La dixième version intermédiaire, 6.0.10, a été publiée le
      <a href="$(HOME)/News/2014/20140719">19 juillet 2014</a>.</li>
</ul>

<ifeq <current_release_squeeze> 6.0.0 "

<p>
À l'heure actuelle, il n'y a pas de mise à jour pour Debian 6.0.
</p>" "

<p>
Veuillez consulter le <a
href="http://http.us.debian.org/debian/dists/squeeze/ChangeLog">journal des
modifications</a> pour obtenir les détails des modifications entre la
version&nbsp;6.0.0 et la version&nbsp;<current_release_squeeze/>.
</p>"/>

<p>
Les corrections apportées à la version stable de la distribution passent
souvent par une période de test étendue avant d'être acceptées dans l'archive.
Cependant, ces corrections sont disponibles dans le répertoire <a
href="http://ftp.debian.org/debian/dists/squeeze-proposed-updates/">\
dists/squeeze-proposed-updates</a> de tout miroir de l'archive Debian.
</p>

<p>
Si vous utilisez APT pour mettre à jour vos paquets, vous pouvez installer les
mises à jour proposées en ajoutant la ligne suivante dans votre fichier
<tt>/etc/apt/sources.list</tt>&nbsp;:
</p>

<pre>
  \# Ajouts proposés pour une version intermédiaire 6.0
  deb http://ftp.us.debian.org/debian squeeze-proposed-updates main contrib non-free
</pre>

<p>
Ensuite, lancez <kbd>apt-get update</kbd> suivi de <kbd>apt-get upgrade</kbd>.
</p>

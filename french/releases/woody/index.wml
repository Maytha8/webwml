#use wml::debian::template title="Informations sur la version Debian GNU/Linux 3.0 « Woody »" BARETITLE="true"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="6933c146d6b5dc1ef12d968ac264529d8ab5df51" maintainer="Jean-Paul Guillonneau"

<p>Debian GNU/Linux_3.0 (<em>Woody</em>) a été publiée le 19 juillet 2002.

Cette version comprend de nombreuses modifications décrites dans notre
<a href="$(HOME)/News/2002/20020719">communiqué de presse</a> et les
<a href="releasenotes">notes de publication</a>.</p>

<p><strong>Debian GNU/Linux 3.0 a été remplacée par
<a href="../sarge/">Debian GNU/Linux 3.1 (<q>Sarge</q>)</a>.
La prise en charge des mises à jour de sécurité a cessé depuis la fin juin 2006.</strong></p>

<p>Debian GNU/Linux 3.0 est disponible <a href="$(DISTRIB)/">à partir
d’Internet</a> et auprès de <a href="$(HOME)/CD/vendors/">fournisseurs de CD</a>.</p>

<p>Avant d’installer Debian, veuillez lire le guide d’installation. Celui-ci
contient pour votre architecture des instructions et des liens pour tous les
fichiers nécessaires pour cette installation.</p>

<p>les architectures suivantes étaient prises en charge dans cette publication :</p>

<ul>
<li><a href="../../ports/alpha/">Alpha</a>
<li><a href="../../ports/arm/">ARM</a>
<li><a href="../../ports/hppa/">HP PA-RISC</a>
<li><a href="../../ports/i386/">PC 32 bits (i386)</a>
<li><a href="../../ports/ia64/">Intel Itanium IA-64</a>
<li><a href="../../ports/m68k/">Motorola 680x0</a>
<li><a href="../../ports/mips/">MIPS (gros-boutiste)</a>
<li><a href="../../ports/mips/">MIPS (petit-boutiste)</a>
<li><a href="../../ports/powerpc/">PowerPC</a>
<li><a href="../../ports/s390/">IBM S/390</a>
<li><a href="../../ports/sparc/">SPARC</a>
</ul>

<p>
Contrairement à nos souhaits, certains problèmes pourraient toujours exister
dans cette version, même si elle est déclarée <em>stable</em>. Nous avons
réalisé <a href="errata">une liste des principaux problèmes connus</a>, et vous
pouvez toujours nous signaler d'autres problèmes.
</p>

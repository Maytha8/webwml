#use wml::debian::template title="Distributie-archief"
#use wml::debian::translation-check translation="b4319ab75a5556391ce61af8f3457785e5efdbe7"
#use wml::debian::toc

# Translator:       Bas Zoetekouw <bas@debian.org>
# Translation Date: Tue Jun 18 20:46:42 CEST 2002

<toc-display />

<toc-add-entry name="old-archive">debian-archief</toc-add-entry>

<p>Als u een van de oude distributies van Debian nodig heeft, kunt u
deze vinden in <a href="https://archive.debian.org/debian-archive/">het
archief van Debian</a> op
<tt>https://archive.debian.org/debian-archive/</tt>.</p>

<p>Releases worden daar bewaard in de <tt>dists/</tt>-map onder
hun codenaam:</p>
<ul>
  <li><a href="../releases/stretch/">stretch</a> is Debian 9</li>
  <li><a href="../releases/jessie/">jessie</a> is Debian 8.0</li>
  <li><a href="../releases/wheezy/">wheezy</a> is Debian 7.0</li>
  <li><a href="../releases/squeeze/">squeeze</a> is Debian 6.0</li>
  <li><a href="../releases/lenny/">lenny</a>   is Debian 5.0</li>
  <li><a href="../releases/etch/">etch</a>    is Debian 4.0</li>
  <li><a href="../releases/sarge/">sarge</a>   is Debian 3.1</li>
  <li><a href="../releases/woody/">woody</a>   is Debian 3.0</li>
  <li><a href="../releases/potato/">potato</a> is Debian 2.2</li>
  <li><a href="../releases/slink/">slink</a>   is Debian 2.1</li>
  <li><a href="../releases/hamm/">hamm</a>     is Debian 2.0</li>
  <li>bo   is Debian 1.3</li>
  <li>rex  is Debian 1.2</li>
  <li>buzz is Debian 1.1</li>
</ul>

<p>We hebben alleen broncode van releases ouder dan bo, en binaire bestanden en broncode van bo en nieuwere releases.
Naarmate de tijd verstrijkt, zullen de binaire pakketten van oude releases vervallen.</p>

<p>Als u APT gebruikt, zijn de relevante regels voor
<tt>sources.list</tt> bijvoorbeeld als volgt:</p>
<pre>
  deb http://archive.debian.org/debian/ hamm contrib main non-free
</pre>
<p>of</p>
<pre>
  deb http://archive.debian.org/debian/ bo bo-unstable contrib main non-free
</pre>

<p>toegang met rsync is beschikbaar via <pre>rsync.archive.debian.org</pre></p>

<p>Hieronder volgt een lijst van spiegelservers die ook het archief hebben
opgenomen in hun collectie:</p>

#include "$(ENGLISHDIR)/distrib/archive.mirrors"
<archivemirrors>

<toc-add-entry name="non-us-archive">debian-non-US-archief</toc-add-entry>

<p>In het verleden was er software in Debian-pakketten beschikbaar die niet
in de Verenigde Staten (en andere landen) kon worden gedistribueerd in
verband met restricties op de uitvoer van cryptografie of softwarepatenten.
Hiervoor beheerde Debian een speciaal archief, genaamd <q>non-US</q>.</p>

<p>Met Debian 3.1 zijn deze pakketten opgenomen in het reguliere archief
(<q>main</q>) en sindsdien bestaat het archief debian-non-US niet meer. Het is
nu echt <em>gearchiveerd</em> en opgenomen in het archief
archive.debian.org.</p>

<p>De pakketten zijn nog steeds beschikbaar op de server
archive.debian.org. Beschikbare toegangsmethoden zijn:</p>
<blockquote><p>
<a href="https://archive.debian.org/debian-non-US/">https://archive.debian.org/debian-non-US/</a><br>
rsync://rsync.archive.debian.org/debian-non-US/
</p></blockquote>

<p>De relevante regels voor sources.list om de pakketten met APT te gebruiken,
zijn:</p>

<pre>
  deb http://archive.debian.org/debian-non-US woody/non-US main contrib non-free
  deb-src http://archive.debian.org/debian-non-US woody/non-US main contrib non-free
</pre>

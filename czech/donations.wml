#use wml::debian::template title="Sponzorství"
#use wml::debian::translation-check translation="ff49b18e1e0e4a97067fd8780a8e6e6b7ab8458d" maintainer="Michal Simunek"

<p>
Sponzorské dary jsou spravovány
<a href="$(HOME)/devel/leader">Projektovým vedoucím Debianu</a> (DPL)
a umožňuje Debianu mít
<a href="https://db.debian.org/machines.cgi">zařízení</a>,
<a href="https://wiki.debian.org/Teams/DSA/non-DSA-HW">další hardware</a>,
domény, kryptografické certifikáty,
<a href="https://www.debconf.org">konferenci Debianu</a>,
<a href="https://wiki.debian.org/MiniDebConf">mini-konference Debianu</a>,
<a href="https://wiki.debian.org/Sprints">vývojové sprinty</a>,
účastnit se na dalších akcích
a další věci.
Děkujeme všem našim <a href="#donors">sponzorům</a> za to, že podporují Debian!
</p>

<p id="default">
Nejjednodušším způsobem jak podpořit Debian je skrze organizaci 
<a href="https://www.spi-inc.org/" title="SPI">Software in the Public Interest</a> pomocí Paypal.
Jedná se o neziskovou organizaci, která spravuje svěřený majetek pro Debian.
Podporu můžete také provést <a href="#methods">níže uvedenými způsoby</a>.
<!--
You can donate via the <a href="#methods">methods listed below</a>.
-->
</p>

<form action="https://www.paypal.com/cgi-bin/webscr" method="post">
<div>
<input type="hidden" name="cmd" value="_s-xclick">
<input type="hidden" name="hosted_button_id" value="86AHSZNRR29UU">
<input type="submit" value="Donate via PayPal" />
</div>
</form>

<!--
Hidden due to an issue with USAePay/Paysimple
https://lists.debian.org/msgid-search/CABJgS1ZtpcfM3uzA+j9_mp6ocR2H=p4cOcWWs2rjSNwhWYWepw@mail.gmail.com
<form method="post" action="https://www.usaepay.com/interface/epayform/">
<div>
  <span>$<input type="text" name="UMamount" size="6"> USD, paid once only</span>
  <input name="UMdescription" value="Debian general contribution" type="hidden">
  <input name="UMkey" value="ijWB0O98meGpX0LrCP0eb1Y94sI1Dl67" type="hidden">
  <input name="UMcommand" value="sale" type="hidden" />
  <input type="submit" tabindex="2" value="Donate" />
</div>
</form>
-->

<h2 id="methods">Možnosti sponzorství</h2>

<p>
Různé <a href="https://wiki.debian.org/Teams/Treasurer/Organizations">organizace</a>
spravují svěřený majetek pro Debian a jsou příjemci podpory jménem Debianu.
</p>

<table>
<tr>
<th>Organizace</th>
<th>Metody</th>
<th>Poznámky</th>
</tr>
<tr>
<td><a href="#spi"><acronym title="Software in the Public Interest">SPI</acronym></a></td>
<td>
 <a href="#spi-paypal">PayPal</a>,
 <!--
Hidden due to an issue with USAePay/Paysimple
https://lists.debian.org/msgid-search/CABJgS1ZtpcfM3uzA+j9_mp6ocR2H=p4cOcWWs2rjSNwhWYWepw@mail.gmail.com
 <a href="#spi-usa-epay">USA ePay</a>,
-->
 <a href="#spi-click-n-pledge">Click &amp; Pledge</a> (opakované dary),
 <a href="#spi-cheque">šek</a> (USD/CAD),
 <a href="#spi-other">ostatní</a>
</td>
<td>USA, nezisková organizace, osvobozená od daně</td>
</tr>
<tr>
<td><a href="#debianfrance">Debian France</a></td>
<td>
 <a href="#debianfrance-bank">bankovním převodem</a>,
 <a href="#debianfrance-paypal">PayPal</a>
</td>
<td>Francie, nezisková organizace, osvobozená od daně</td>
</tr>
<tr>
<td><a href="#debianch">debian.ch</a></td>
<td>
 <a href="#debianch-bank">bankovním převodem</a>,
 <a href="#debianch-other">ostatní</a>
</td>
<td>Švýcarsko, nezisková organizace</td>
</tr>
<tr>
<td><a href="#debian">Debian</a></td>
<td>
 <a href="#debian-equipment">vybavení</a>,
 <a href="#debian-time">čas</a>,
 <a href="#debian-other">ostatní</a>
</td>
<td></td>
</tr>

# Template:
#<tr>
#<td><a href="#"><acronym title=""></acronym></a></td>
#<td>
# <a href="#"></a>,
# <a href="#"></a> (dovoluje opakované dary),
# <a href="#cheque"></a> (CUR)
#</td>
#<td>, nezisková organizace, osvobozená od daně</td>
#</tr>

</table>

<h3 id="spi">Software in the Public Interest</h3>

<p>
<a href="https://www.spi-inc.org/" title="SPI">Software in the Public Interest, Inc.</a>
je nezisková organizace, osvobozená od daně se sídlem ve Spojených státech Amerických
založená lidmi z Debianu v roce 1997 za účelem pomoci organizacím, věnujícím se svobodnému softwaru/hardwaru.
</p>

<h4 id="spi-paypal">PayPal</h4>
<p>
Jednorázové a opakované dary mohou být provedeny skrze
<a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&amp;hosted_button_id=86AHSZNRR29UU">stránku SPI</a>
na webu PayPalu.
Pro opakované dary, prosíme, zaškrtněte políčko
"Make this a monthly donation" (tj. vytvořit dar, opakující se měsíčně).
</p>

<h4 id="spi-click-n-pledge">Click &amp; Pledge</h4>

<p>
Jednorázové a opakované dary mohou být provedeny skrze
<a href="https://co.clickandpledge.com/advanced/default.aspx?wid=34115">stránku SPI</a>
na webu Click &amp; Pledge.
Pro opakované dary,
prosíme vyberte jak často chcete přispívat v políčku na pravé straně,
posuňte se níže na "Debian Project Donation",
vložte částku, kterou chcete darovat,
klikněte na políčko "Add to cart" (tj. vložit do košíku)
a dále postupujte podle instrukcí na stránce.
</p>

<!--
Unfortunately this isn't possible to do yet:
<form method="post" action="https://co.clickandpledge.com/advanced/default.aspx?wid=34115">
<input name="ScriptManager1" value="UpdatePanel1|btnDAddToCart_dnt_33979" type="hidden">
$<input name="DAmount_33979" size="6" id="DAmount_33979" type="text"> USD, paid
<select name="cboxRecurring" id="cboxRecurring">
  <option value="-1">once only</option>
  <option value="0">weekly</option>
  <option value="1">fortnightly</option>
  <option value="2">monthly</option>
  <option value="3">every 2 months</option>
  <option value="4">quarterly</option>
  <option value="5">bi-annually</option>
  <option value="6">annually</option>
</select>
<input type="submit" name="btnDAddToCart_dnt_33977" id="btnDAddToCart_dnt_33977" value="Donate"/>
</form>
-->

<h4 id="spi-cheque">Šek</h4>

<p>
Dary mohou být provedeny skrze šek, nebo peněžní převod v
<abbr title="US dollars">Amerických dolarech</abbr> a
<abbr title="Canadian dollars">Kanadských dolarech</abbr>.
Prosíme, vložte Debian do pole pro poznámku a pošlete šeky do SPI na adresu
uvedenou na <a href="https://www.spi-inc.org/donations/">stránce sponzorství webu SPI</a>.
</p>

<h4 id="spi-other">Ostatní</h4>

<p>
Je také možné provést dary skrze bankovní převod a ostatními metodami.
V některých částech světa může být jednodušší dát sponzorský dar
jedné z partnerských organizací Software in the Public Interest.
Pro více informací, prosíme, navštivte
<a href="https://www.spi-inc.org/donations/">stránky sponzorství webu SPI</a>.
</p>

<h3 id="debianfrance">Debian France</h3>

<p>
<a href="https://france.debian.net/">Asociace Debian France</a> je
organizace, registrovaná  ve Francii v rámci <q>zákona 1901</q>,
založena na podporu a propagaci
Projektu Debian ve Francii.
</p>

<h4 id="debianfrance-bank">Bankovní převod</h4>
<p>
Je možné přispět darem skrze bankovní převod na účet uvedený na
<a href="https://france.debian.net/soutenir/#compte">stránce sponzorství webu Debian France</a>. Potvrzení o daru můžete získat, když napíšete email na
<a href="mailto:donation@france.debian.net">donation@france.debian.net</a>.
</p>

<h4 id="debianfrance-paypal">PayPal</h4>
<p>
Dary je možné posílat skrze
<a href="https://france.debian.net/galette/plugins/paypal/form">stránku Debian France na webu PayPal</a>.
Tyto dary mohou být určeny přímo pro Debian France, nebo obecně pro Projekt Debian.
</p>

<h3 id="debianch">debian.ch</h3>

<p>
<a href="https://debian.ch/">debian.ch</a> byla založena aby reprezentovala
Projekt Debian ve Švýcarsku a v Lichtenštejnském knížectví.
</p>

<h4 id="debianch-bank">Bankovní převod</h4>

<p>
Je možné přispět darem pomocí Švýcarské nebo mezinárodní banky 
převodem na účty, uvedené na 
<a href="https://debian.ch/">webové stránce debian.ch</a>.
</p>

<h4 id="debianch-other">Ostatní</h4>

<p>
Pro dary, které budou provedeny ostatními způsoby můžete kontaktovat sponzorské adresy
umístěné na <a href="https://debian.ch/">webové stránce debian.ch</a>.
</p>

# Template:
#<h3 id=""></h3>
#
#<p>
#</p>
#
#<h4 id=""></h4>
#
#<p>
#</p>

<h3 id="debian">Debian</h3>

<p>
Debian může přijmout dar ve formě <a href="#debian-equipment">vybavení</a>.
V současné době nemůže přijímat <a href="#debian-other">jiné</a> dary.
</p>

<a name="equipment_donations"></a>
<h4 id="debian-equipment">Vybavení a služby</h4>

<p>
Debian také závisí na darech ve formě vybavení a služeb od jednotlivců,
společností, univerzit atd., díky kterým může být ve spojení se zbytkem světa.
</p>

<p>Pokud má vaše společnost nevyužité zařízení, nebo náhradní vybavení (pevné disky, 
řadiče SCSI, síťové karty, apod), které vám zabírají místo, zvažte možnost darovat je 
Debianu. Prosíme kontaktujte našeho
<a href="mailto:hardware-donations@debian.org">delegáta hardwarových darů</a>,
aby jste se dozvěděli více podrobností.</p>

<p>Debian spravuje <a href="https://wiki.debian.org/Hardware/Wanted">seznam potřebného hardwaru</a>, který je nutný
pro různé služby a skupiny v rámci projektu.</p>

<h4 id="debian-time">Čas</h4>

<p>
Je mnoho způsobu, jak můžete <a href="$(HOME)/intro/help">pomoci Debianu</a>
ve svém volném, nebo pracovním čase.
</p>

<h4 id="debian-other">Další</h4>

<p>
Debian v současné době nemůže přijímat žádné kryptoměny, ale 
hledáme způsob jak tuto metodu darování zpřístupnit.
# Brian Gupta requested we discuss this before including it:
#Pokud máte kryptoměny, které chcete darovat, nebo nápady, které chcete sdílet
#můžete se spojit s <a href="mailto:madduck@debian.org">Martinem f. krafft</a>.
</p>

<h2 id="donors">Sponzoři</h2>

<p>Níže je uveden seznam organizací, které poskytly Debianu dar ve formě vybavení, nebo služeb:</p>

<ul>
  <li><a href="https://db.debian.org/machines.cgi">sponzorství hostingu, nebo hardwaru</a></li>
  <li><a href="mirror/sponsors">sponzorství síťových zrcadel</a></li>
  <li><a href="partners/">vývojoví a servisní partneři</a></li>
</ul>

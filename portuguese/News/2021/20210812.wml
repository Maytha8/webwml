#use wml::debian::translation-check translation="a48c50faef33185d0ae2746854bc99435817bad1"
<define-tag pagetitle>O Projeto Debian lamenta a perda de Robert Lemmen, Karl Ramm e Rogério Theodoro de Brito</define-tag>
<define-tag release_date>2021-08-12</define-tag>
#use wml::debian::news

<p>
O Projeto Debian perdeu vários membros de sua comunidade neste último ano.
</p>

<p>
Em junho de 2020, Robert Lemmen faleceu após uma doença grave.
Robert participava regularmente dos encontros Debian Munich desde o início dos
anos 2000 e ajudava com estandes locais.
Ele era um Desenvolvedor Debian desde 2007. Entre outras contribuições, ele
empacotou módulos para Raku (Perl6 naquela época) e ajudou outros(as)
contribuidores(as) a se envolverem na Equipe Raku.
Ele também se esforçou para rastrear dependências circulares no Debian. 
</p>

<p>
Karl Ramm faleceu em junho de 2020, após complicações devido ao câncer de cólon
metastático.
Ele era um Desenvolvedor Debian desde 2001 e empacotou vários componentes do
<a href="https://en.wikipedia.org/wiki/Project_Athena"> Projeto Athena</a> do
MIT.
Ele era apaixonado por tecnologia e Debian, e sempre interessado em ajudar
outras pessoas a encontrar e promover suas paixões. 
</p>

<p>
Em abril de 2021, perdemos o brasileiro Rogério Theodoro de Brito devido a
pandemia do COVID-19.
Rogério gostava de programar pequenas ferramentas e foi um contribuidor Debian
por mais de 15 anos.
Entre outros projetos, ele contribuiu para o uso de dispositivos
Kurobox/Linkstation no Debian e manteve a ferramenta youtube-dl. Ele também
participou e foi o <q>contato do Debian</q> em vários projetos upstream. 
</p>

<p>
O Projeto Debian homenageia o bom trabalho de Robert, Karl e Rogerio e sua
forte dedicação ao Debian e ao Software Livre. Suas contribuições não serão
esquecidas, e os altos padrões de seu trabalho continuarão a servir de
inspiração para outras pessoas. 
</p>

<h2>Sobre o Debian</h2>
<p>O projeto Debian é uma associação de desenvolvedores(as) de Software Livre
que dedicam seu tempo e esforço como voluntários(as) para produzir o sistema
operacional livre Debian.</p>

<h2>Informações de contato</h2>
<p>Para mais informações, por favor visite as páginas web do Debian em
<a href="$(HOME)/">https://www.debian.org/</a>, envie um e-mail (em inglês) para
&lt;<a href="mailto:press@debian.org">press@debian.org</a>&gt;.</p>

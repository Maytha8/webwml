#use wml::debian::template title="Informações de lançamento do Debian &ldquo;sarge&rdquo;"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="c828123435e049f9c2b89a8e80a66d0817b40439"

<p>O Debian GNU/Linux 3.1 (a.k.a. <em>sarge</em>) foi
lançado dia 6 de junho de 2005. A nova versão inclui muitas
alterações importantes, descritas
em nosso <a href="$(HOME)/News/2005/20050606">comunicado à imprensa</a> e
nas <a href="releasenotes">notas de lançamento</a>.</p>

<p><strong>O Debian GNU/Linux 3.1 foi substituído pelo
<a href="../etch/">Debian GNU/Linux 4.0 (<q>etch</q>)</a>.
Atualizações de segurança foram descontinuadas no final de março de 2008.
</strong></p>

<p>Para obter e instalar o GNU/Linux 3.1, veja
a página de informações de instalação e o guia de instalação</a>. Para
atualizar de uma versão mais antiga do Debian, veja as instruções nas
<a href="releasenotes">notas de lançamento</a>.</p>

<p>As seguintes arquiteturas de computadores são suportadas nesta versão:</p>

<ul>
<li><a href="../../ports/alpha/">Alpha</a>
<li><a href="../../ports/arm/">ARM</a>
<li><a href="../../ports/hppa/">HP PA-RISC</a>
<li><a href="../../ports/i386/">32-bit PC (i386)</a>
<li><a href="../../ports/ia64/">Intel Itanium IA-64</a>
<li><a href="../../ports/m68k/">Motorola 680x0</a>
<li><a href="../../ports/mips/">MIPS (big endian)</a>
<li><a href="../../ports/mipsel/">MIPS (little endian)</a>
<li><a href="../../ports/powerpc/">PowerPC</a>
<li><a href="../../ports/s390/">IBM S/390</a>
<li><a href="../../ports/sparc/">SPARC</a>
</ul>

<p>Apesar dos nossos desejos, podem existir alguns problemas nesta versão,
embora ela tenha sido declarada estável (stable). Fizemos
<a href="errata">uma lista dos problemas conhecidos mais importantes</a>,
e você sempre pode relatar outros problemas para nós.</p>

#use wml::debian::template title="Logotipos de Debian" BARETITLE=true
#include "$(ENGLISHDIR)/logos/index.data"
#use wml::debian::translation-check translation="97732ca8593e39ce8b981adf7f81657417b62c73" maintainer="Laura Arjona Reina"

<p>Debian tiene dos logotipos. El <a href="#open-use">logotipo oficial</a> (también conocido como
<q>logotipo de uso libre</q>) contiene la conocida <q>espiral</q> de Debian 
y representa de la mejor manera la identidad visual del proyecto Debian. También existe
un <a href="#restricted-use">logotipo de uso restringido</a> separado, que es usado sólo por el proyecto
Debian y sus miembros. Para referirse a Debian, por favor use el logotipo de uso libre.

<hr>

<table cellspacing="0" cellpadding="5" width="100%">
<colgroup span="2">
<col width="65%" />
<col width="35%" />
</colgroup>

<tr>
<th colspan="2"><a name="open-use">Logotipo de Debian de uso libre</a></th>
</tr>
<tr>
<td>

<p>El logotipo de uso libre de Debian viene en dos variantes, con y sin 
etiqueta &ldquo;Debian&rdquo;.</p>

<p>El/los logo(s) son Copyright (c) 1999 
<a href="https://www.spi-inc.org/">Software in the Public Interest, 
Inc.</a>,
y se publica(n) bajo los términos de la 
<a href="https://www.gnu.org/copyleft/lgpl.html">Licencia GNU Lesser General Public</a>, 
versión 3 o posterior, o, si lo prefiere, de la 
<a href="https://creativecommons.org/licenses/by-sa/3.0/">Licencia Creative Commons
Attribution-ShareAlike 3.0 Unported</a>.</p>

<p>Nota: apreciaríamos que hiciese un enlace en la imagen a
 <a href="$(HOME)">https://www.debian.org/</a> si la usa en una página web.</p>
</td>
<td>
<openlogotable>
</td>
</tr>
</table>

<hr/>

<table cellspacing="0" cellpadding="5" width="100%">
<colgroup span="2">
<col width="65%" />
<col width="35%" />
</colgroup>
<tr>
<th colspan="2"><a name="restricted-use">Logotipo de Debian de uso restringido</a></th>
</tr>
<tr>
<td>
<h3>Licencia del logotipo de Debian de uso restringido</h3>

<p>Copyright (c) 1999 Software in the Public Interest</p>
<ol>
  <li>Este logotipo sólo puede utilizarse si:
    <ul>
      <li>el producto para el que se usa está hecho siguiendo un
        proceso documentado publicado en www.debian.org (por ejemplo,
        creación de CD oficiales), o</li>
      <li>Debian da su aprobación oficial para su uso en un asunto
        específico.</li>
    </ul></li>
  <li>Puede usarse si una parte oficial de Debian (decidido usando las
    reglas en 1) es parte del producto completo, o si se aclara que
    solo esta parte ha sido aprobada oficialmente.</li>
  <li>Nos reservamos el derecho a revocar esta licencia para un producto.</li>
</ol>
<p>Se permite usar el logotipo oficial en artículos de vestir (camisetas,
gorras, etc..) siempre y cuando las haya realizado un
desarrollador de Debian y no se vendan para conseguir beneficios.
</td>
<td>
<officiallogotable>
</td>
</tr>
</table>

<hr/>

<h2>Acerca de los logotipos de Debian</h2>
 <p>
 Los logotipos de Debian fueron seleccionados por votación de los desarrolladores de Debian en 1999.
Fueron diseñados por <a href="mailto:rsilva@debian.org">Raul Silva</a>. El color rojo
utilizado en el tipo de letra es, nominalmente, <strong>Rubine Red 2X CVC</strong>. Su equivalente actual es,
o bien PANTONE Strong Red C (representado en RGB como #CE0056), o bien PANTONE Rubine Red C
(representado en RGB como #CE0058). Puede encontrar más detalles sobre los logotipos, sobre el motivo
por el que se ha reemplazado el PANTONE Rubine Red 2X CVC y sobre otros equivalentes del color rojo
en la <a href="https://wiki.debian.org/DebianLogo">página wiki del logotipo de Debian</a>.</p>

<h2>Otras imágenes promocionales</h2>

<h3>Botones de Debian</h3>

<p><img class="ico" src="button-1.gif" alt="[Debian GNU/Linux]">
Este es el primer botón hecho para el Proyecto. La licencia para este
logotipo es equivalente a la del Uso libre. Este logotipo fue creado por 
<a href="mailto:csmall@debian.org">Craig Small</a>.

<p>Aquí hay algunos botones más que se han hecho para Debian.
<br />
<morebuttons>

<h3>Logo de diversidad en Debian</h3>

<p>Hay una variante del logo de Debian para promocionar la diversidad en nuestra
comunidad, llamada logo de diversidad en Debian:
<br/>
<img src="diversity-2019.png" alt="[logo de diversidad en Debian]" />
<br/>
El logo fue creado por <a href="https://wiki.debian.org/ValessioBrito">Valessio Brito</a>
y tiene licencia GPLv3.
La fuente (en formato SVG) está disponible en el <a href="https://gitlab.com/valessiobrito/artwork">repositorio Git</a> del autor.
<br />
</p>

<h3>Pegatina hexagonal de Debian</h3>

<p>Esto es una pegatina que sigue la
<a href="https://github.com/terinjokes/StickerConstructorSpec">especificación de pegatinas hexagonales</a>:
<br/>
<img src="hexagonal.png" alt="[Debian GNU/Linux]" />
<br/>
En el <a href="https://salsa.debian.org/debian/debian-flyers/tree/master/hexagonal-sticker">repositorio «debian-flyers»</a>
están disponibles la fuente (en formato SVG) y un fichero
Makefile para generar las previsualizaciones png y svg.

La licencia de esta pegatina es equivalente a la licencia del logo de uso libre.
La pegatina fue creada por <a href="mailto:valhalla@trueelena.org">Elena Grandi</a>.</p>
<br />

#
# Logo font: Poppl Laudatio Condensed
#
# https://lists.debian.org/debian-www/2003/08/msg00261.html
#



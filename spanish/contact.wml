#use wml::debian::template title="Cómo contactar con nosotros" NOCOMMENTS="yes" MAINPAGE="true"
#use wml::debian::translation-check translation="fb050641b19d0940223934350c51061fcb2439a9"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
  <li><a href="#generalinfo">Información general</a>
  <li><a href="#installuse">Instalación y uso de Debian</a>
  <li><a href="#press">Publicidad y prensa</a>
  <li><a href="#events">Eventos y conferencias</a>
  <li><a href="#helping">Ayudar a Debian</a>
  <li><a href="#packageproblems">Informar de problemas en paquetes Debian</a>
  <li><a href="#development">Desarrollo de Debian</a>
  <li><a href="#infrastructure">Problemas con la infraestructura de Debian</a>
  <li><a href="#harassment">Problemas de acoso</a>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Le rogamos que haga sus consultas en <strong>inglés</strong>, ya que este es el idioma que hablamos la mayoría de nosotros y nosotras. Si esto no es posible, por favor, pida ayuda en alguna de nuestras <a href="https://lists.debian.org/users.html#debian-user">listas de correo para usuarios</a>, que están disponibles en muchos idiomas.</p>
</aside>

<p>
Debian es una organización grande y hay varias maneras de ponerse en contacto con miembros del proyecto y con otros usuarios y usuarias de Debian. Esta página resume medios de contacto utilizados con frecuencia. No es una relación completa en absoluto, consulte el resto de páginas web para conocer otras formas de contactar.
</p>

<p>
Tenga en cuenta que la mayoría de las direcciones de correo electrónico mostradas en esta página corresponden a listas de correo abiertas y con archivos públicos. Lea la <a href="$(HOME)/MailingLists/disclaimer">nota legal</a> antes de enviar mensajes.
</p>

<h2 id="generalinfo">Información general</h2>

<p>
La mayoría de la información sobre Debian se encuentra en nuestro sitio web: <a href="$(HOME)">https://www.debian.org/</a>, de manera que, por favor, navegue y <a href="$(SEARCH)">busque</a> en él antes de ponerse en contacto con nosotros.
</p>

<p>
Puede enviar pregunas relacionadas con el proyecto Debian a la lista de correo <email debian-project@lists.debian.org>. No haga preguntas generales sobre Linux en esta lista; continúe leyendo para información sobre otras listas de correo.
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="doc/user-manuals#faq">Lea nuestras FAQ</a></button></p>

<aside class="light">
  <span class="fa fa-envelope fa-5x"></span>
</aside>
<h2 id="installuse">Instalación y uso de Debian</h2>

<p>
Si está seguro de que la documentación incluida en el sistema de instalación que usa y en nuestra
web no contiene la solución a su problema, hay una lista de correo muy activa
en la que usuarios y desarrolladores de Debian pueden responder a sus preguntas.
En ella, puede hacer todo tipo de preguntas relativas a:
</p>

<ul>
  <li>Instalación
  <li>Configuración
  <li>Hardware soportado
  <li>Administración de máquinas
  <li>Uso de Debian
</ul>

<p>
<a href="https://lists.debian.org/debian-user/">Suscríbase</a> a
la lista y envíe sus consultas a <email debian-user@lists.debian.org>.
</p>

<p>
Además, hay listas de correo para usuarios hablantes de otros idiomas. Vea la información de suscripción a las <a href="https://lists.debian.org/users.html#debian-user">listas de correo internacionales</a>.
</p>

<p>
Puede navegar por nuestros <a href="https://lists.debian.org/">archivos de listas de correo</a> o <a href="https://lists.debian.org/search.html">hacer búsquedas</a> en ellos sin necesidad de suscribirse.
</p>

<p>
Por si eso fuera poco, es posible navegar por nuestras listas de correo como si se tratara de grupos de noticias.
</p>

<!-- añadido por jfs -->
<p>Si usted es hispanoparlante, puede
<a href="https://lists.debian.org/debian-user-spanish/">suscribirse
a <em>debian-user-spanish</em></a>. A esta última están suscritos usuarios
y desarrolladores de Debian hispanoparlantes, los temas son los mismos
que los de <em>debian-user</em>, pero se tratan en español.
Mande sus preguntas a
<email debian-user-spanish@lists.debian.org>.

<p>
Si cree que ha encontrado un fallo en nuestro sistema de instalación, por favor, escriba a la lista de correo <email debian-boot@lists.debian.org>. También puede registrar un <a href="$(HOME)/releases/stable/i386/ch05s04#submit-bug">informe de fallo</a> en el pseudopaquete <a href="https://bugs.debian.org/debian-installer">debian-installer</a>.
</p>

<aside class="light">
  <span class="far fa-newspaper fa-5x"></span>
</aside>
<h2 id="press">Publicidad y prensa</h2>

<p>
El <a href="https://wiki.debian.org/Teams/Publicity">equipo de publicidad de Debian</a> modera las noticias y anuncios en todos los recursos oficiales de Debian, como, por ejemplo, en algunas listas de correo, en el blog, en el sitio web de micronoticias y en los canales oficiales de medios sociales.
</p>

<p>
Si va a escribir sobre Debian y necesita ayuda o información, póngase en contacto con el <a href="mailto:press@debian.org">equipo de publicidad</a>. Esta es también la dirección correcta si está pensando en enviar contribuciones para nuestra págnia de noticias.
</p>

<aside class="light">
  <span class="fas fa-handshake fa-5x"></span>
</aside>
<h2 id="events">Eventos y conferencias</h2>

<p>
Envíe las invitaciones a <a href="$(HOME)/events/">conferencias</a>, exhibiciones u otros eventos a <email events@debian.org>.
</p>

<p>
Las peticiones de folletos, pósteres y participación en Europa deberían enviarse a la lista de correo <email debian-events-eu@lists.debian.org>.
</p>

<aside class="light">
  <span class="fas fa-hands-helping fa-5x"></span>
</aside>
<h2 id="helping">Ayudar a Debian</h2>

<p>
Hay muchas maneras de apoyar al proyecto Debian y de contribuir a la distribución. Si desea ofrecer ayuda, por favor, antes visite nuestra página sobre <a href="devel/join/">cómo ayudar a Debian</a>.
</p>

<p>Si desea mantener una réplica (<i>mirror</i>) de Debian, vea las páginas
<a href="mirror/">Cómo hacer réplicas</a>. Las nuevas réplicas se añaden usando
<a href="mirror/submit">este formulario</a>. Los problemas con las réplicas ya en marcha
se pueden enviar a <email mirrors@debian.org>.
</p>

<p>Si desea vender los CD o DVD de Debian, vea la <a href="CD/vendors/info">\
información para vendedores de CD</a>. Para aparecer en la lista de distribuidores,
utilice <a href="CD/vendors/adding-form">este formulario</a>.
</p>

<aside class="light">
  <span class="fas fa-bug fa-5x"></span>
</aside>
<h2 id="packageproblems">Informar de problemas en paquetes Debian</h2>

<p>Si desea presentar un informe de fallo sobre un paquete Debian, disponemos de un sistema
de seguimiento de fallos donde puede informar de su problema de una forma sencilla. Lea por favor las
<a href="Bugs/Reporting">instrucciones para rellenar un informe de fallo</a>.</p>

<p>Si simplemente desea comunicarse con la persona o equipo responsable de un paquete
Debian, puede usar el alias de correo electrónico definido para cada paquete.
Cualquier mensaje enviado a &lt;<var>nombre_de_paquete</var>&gt;@packages.debian.org será
reenviado a la persona o equipo que se encarga de él.</p>

<p>Si desea informar discretamente a los desarrolladores de un
problema de seguridad en Debian, envíe un correo electrónico a
<email security@debian.org>.
</p>

<aside class="light">
  <span class="fas fa-code-branch fa-5x"></span>
</aside>
<h2 id="development">Desarrollo de Debian</h2>

<p>Si tiene preguntas sobre el desarrollo de Debian, tenemos
varias <a href="https://lists.debian.org/devel.html">listas de correo</a>
a través de las cuales puede ponerse en contacto con nuestros desarrolladores y desarrolladoras.
</p>

<p>
Hay también una lista general para desarrolladores: <email debian-devel@lists.debian.org>.
Siga este <a href="https://lists.debian.org/debian-devel/">enlace</a> para suscribirse a ella.
</p>

<aside class="light">
  <span class="fas fa-network-wired fa-5x"></span>
</aside>
<h2 id="infrastructure">Problemas con la infraestructura de Debian</h2>

<p>Para informar sobre un problema en un servicio de Debian, habitualmente podrá
<a href="Bugs/Reporting">enviar un informe de fallo</a> indicando el
<a href="Bugs/pseudo-packages">pseudopaquete</a> apropiado.</p>

<p>De forma alternativa, puede enviar un correo electrónico a una de las direcciones siguientes:</p>

<define-tag btsurl>paquete: <a href="https://bugs.debian.org/%0">%0</a></define-tag>

<dl>
<dt>Editores de páginas web</dt>
  <dd><btsurl www.debian.org><br />
      <email debian-www@lists.debian.org></dd>
#include "$(ENGLISHDIR)/devel/website/tc.data"
<ifneq "$(CUR_LANG)" "English" "
<dt>Traductores de páginas web</dt>
  <dd><: &list_translators($CUR_LANG); :></dd>
">
<dt>Administradores de las listas de correo y de los archivos de listas de correo</dt>
  <dd><btsurl lists.debian.org><br />
      <email listmaster@lists.debian.org></dd>
<dt>Administradores del sistema de seguimiento de fallos</dt>
  <dd><btsurl bugs.debian.org><br /> 
      <email owner@bugs.debian.org></dd>
</dl>

<aside class="light">
  <span class="fas fa-umbrella fa-5x"></span>
</aside>
<h2 id="harassment">Problemas de acoso</h2>


<p>Debian es una comunidad de personas que valora el respeto. Si usted es víctima de una comportamiento inadecuado, si alguien del proyecto le ha perjudicado o si se ha sentido acosado o acosada, contacte con el Equipo de comunidad: <email community@debian.org>
</p>

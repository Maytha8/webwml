#use wml::debian::template title="El proyecto de traducción de descripciones de Debian, DDTP"
#use wml::debian::toc
#use wml::debian::translation-check translation="888996d3a5645b9cc81d96ba9a5eb337da2ddc80"

<p>
El <a href="https://ddtp.debian.org">Proyecto de traducción de descripciones de 
Debian</a> (desarrollado por <a 
href="mailto:Michael%20Bramer%20%3Cgrisu@debian.org%3E">Michael Bramer</a>) 
trata de proporcionar las descripciones de los paquetes traducidas y la 
infraestructura para permitir su traducción. Aunque existe desde hace unos años, 
estuvo desactivado después de una avería en un servidor de Debian y actualmente 
sólo tiene las funcionalidades básicas comparándolo con el pasado.
</p>

<p>
El proyecto permite:
</p>
<ul>
  <li>Obtener las descripciones de los paquetes actuales (sid) y anteriores 
.</li>
  <li>Añadir nuevas traducciones mediante una interfaz basada en correo 
electrónico.</li>
  <li>Reutilizar los párrafos ya traducidos de una descripción en otras 
descripciones.</li>
  <li>Proporcionar archivos <tt>Translation-*</tt> para las réplicas y APT.</li>
</ul>

<p>
No se puede traducir las secciones <q>non-free</q> y <q>non-free-firmware</q> del archivo de 
Debian porque pueden existir problemas con las licencias que lo prohíban, como 
p. ej. la traducción y se tiene que comprobar con mucho cuidado.
<!-- I thought that even for non-free projects the debian packaging stuff
     (debian/) is free but it seems it''s not required!? -->
</p>

<p>
Traducir más de 56.000 transcripciones de paquetes es un gran desafío. Por 
favor, colabore para conseguir nuestro objetivo. También puede ver nuestra <a 
href="#todo">lista de tareas pendientes</a>.
</p>

<toc-display/>

<toc-add-entry>Interfaces del DDTP</toc-add-entry>

<p>
Ya que todas las interfaces usan el sistema de DDTP, primero necesita asegurarse 
de que su idioma está activado. La mayoría de los idiomas deberían de estarlo. 
Si no fuera así, escriba a <email debian-i18n@lists.debian.org> para que su 
idioma se pueda activar. 
</p>

<h3 id="DDTSS">La interfaz web</h3>
<p>
Existe una estupenda 
interfaz web denominada <a 
href="https://ddtp.debian.org/ddtss/index.cgi/xx">DDTSS</a> desarrollada por <a 
href="mailto:Martijn%20van%20Oosterhout%20%3Ckleptog@gmail.com%3E">Martijn van 
Oosterhout</a> que intenta simplificar el trabajo de traducción y revisión.
</p>

<h4>Resumen</h4>
<p>
Permite usar todas las funcionalidades de la interfaz de correo electrónico y 
además permite corregir las traducciones. Permite usar una configuración 
personalizada para cada equipo de traducción de modo que puedan decidir cuantas 
revisiones son necesarias para que la descripción se transfiera al DDTP. También 
es posible pedir autorización de usuarios, de modo que sólo un grupo restringido 
de gente pueda realizar ciertas acciones. Además no hace falta tener en cuenta 
la codificación, ya que el DDTSS la gestiona automáticamente.
</p>

<p>
Actualmente, los valores predeterminados de las propiedades son:
</p>
<dl>
  <dt>número de revisiones:</dt><dd>3</dd>
  <dt>idiomas activos:</dt><dd>todos los del DDTP</dd>
  <dt>autorización de usuarios:</dt><dd>desactivada, está abierto para 
todos</dd>
</dl>

<p>
Es posible especificar una lista de palabras predeterminadas para un idioma. 
Esta se usa para proporcionar traducciones predeterminadas mediante mensajes de 
ayuda. Esta lista está disponible mediante un enlace al final de la página del 
idioma.
</p>

<h4>Flujo de trabajo</h4>
<p>
El DDTSS proporciona los siguientes elementos para todos los idiomas:
</p>

<h5>Traducciones pendientes</h5>
<p>
Una lista de traducciones pendientes. Estas son descripciones libremente 
seleccionables para que el usuario las traduzca. Son similares a:
</p>
<pre>
exim4 (priority 52)
exim4-config (priority 52)
ibrazilian (priority 47, busy)
postgresql-client (priority 47)
postgresql-contrib (priority 47)
grap (priority 45)
</pre>

<p>
Un equipo de traducción debería de intentar traducir los paquetes con una 
prioridad más alta (esta se calcula usando la categoría, por ejemplo: esencial, 
básico, ...) primero. Los paquetes se ordenan para realizar las traducciones de 
esta manera.
</p>

<p>
Las descripciones marcadas como <q>busy</q> las tiene reservadas un usuario y no 
se pueden seleccionar durante un tiempo máximo de 15 minutos. Si no se envía 
durante este tiempo se vuelve a marcar como disponible.
</p>

<p>
Una descripción necesita estar completamente traducida antes de que el sistema 
la acepte. De modo que asegúrese de que puede traducir el texto entero antes de 
comenzar. Pulse <q>Submit</q> para añadir la traducción y <q>Abandon</q> si 
decide no traducirla. También es posible que tenga suerte y haya una traducción 
de una versión anterior de la plantilla en inglés junto con una lista de cambios 
de la traducción en inglés que tiene que integrar en su traducción.
Puede copiar y pegar esta vieja traducción desde la parte de abajo de la página 
y actualizarla apropiadamente.
</p>

<p>
# Does not yet work as expected
Para evitar la fluctuación del ancho del texto se sugiere que no introduzca 
líneas nuevas manualmente a menos que sea necesario (como para los elementos de 
las listas). Las líneas se rompen automáticamente. Recuerde que un usuario 
podría añadir o eliminar partes menores durante una corrección que podrían 
resultar en una longitud inconsistente de la línea. Corrigiendo esto hace que la 
lista de cambios creada por la revisión sea difícil de leer.
</p>

<p>
También se pueden seleccionar los paquetes preferidos por su nombre. Esto es 
útil para traducir un conjunto de paquetes similar, como manpages-de, 
manpages-es, para poder copiar y pegar las traducciones anteriores.
</p>

<p>
Incluso los paquetes ya traducidos se pueden obtener de esta manera, para 
mejorarlos (por favor, tenga en cuenta que la funcionalidad actual del DDTP para 
esto es un poco mala, de modo que, si es posible, por ahora intente evitarla).
</p>

<h5>Revisiones pendientes</h5>
<p>
Una lista de descripciones traducidas que todavía necesitan revisarse. Esta 
lista es similar a la siguiente:
</p>

<pre>
 1. aspell-es (needs review, had 1)
 2. bookmarks (needs initial review)
 3. doc-linux-ja-html (needs initial review)
 4. doc-linux-ja-text (needs initial review)
 5. gnome-menus (needs initial review)
 6. geany (needs review, had 2)
 7. initramfs-tools (needs initial review)
 8. inn2 (needs initial review)
</pre>

<p>
Existen las siguientes etiquetas:</p>
<dl>
    <dt lang="en">needs initial review:</dt>
    <dd>La versión actual de esta traducción no ha pasado todavía ninguna 
revisión.</dd>

    <dt lang="en">needs review:</dt>
    <dd>La actual versión de esta traducción necesita más revisiones, pero ya ha 
pasado al menos una.</dd>

    <dt lang="en">reviewed:</dt>
    <dd>Esta descripción se ha revisado sin realizarle ningún cambio. Deben 
revisarla otros usuarios.</dd>

    <dt lang="en">owner:</dt>
    <dd>Esta descripción se tradujo o modificó durante una corrección realizada 
por el usuario. Deben revisarla otros usuarios.</dd>
</dl>

<p>
Si se ha realizado una revisión con correcciones, tendrá una lista de cambios 
coloreados que mostrarán todos los cambios desde la última revisión del paquete.
</p>

<h5>Últimas traducciones</h5>
<p>
Una lista de las descripciones transferidas al DDTP. Como mucho se mostrarán 
veinte paquetes junto a la fecha de la transferencia.
</p>

<h3 id="Pootle">La interfaz de internacionalización</h3>
<p>
Ha habido planes para implementar un nuevo entorno para las traducciones de 
varios documentos en Debian, como archivos PO y plantillas de Debconf. Esto 
también debería permitir la traducción de las descripciones de paquetes en algún 
momento. Si se activa y queda funcionando como se espera, el actual DDTP y sus 
interfaces se desactivarán.
</p>

<p>
Este entorno se basaría en 
<a href="http://pootle.locamotion.org/">Pootle</a>. Fue un <a 
href="https://wiki.debian.org/SummerOfCode2006?#Translation_Coordination_System"
>proyecto</a> del Google Summer of Code.
</p>

<toc-add-entry name="rules">Reglas comunes de traducción</toc-add-entry>
<p>
Es importante que no cambie las descripciones en inglés durante la traducción. 
Si descubre errores en ellas debería de enviar un informe de fallo al paquete 
correspondiente, para obtener más detalles véase <a 
href="$(HOME)/Bugs/Reporting">Cómo informar de un fallo en Debian</a>.
</p>

<p>
Traduzca las partes sin traducir de cada adjunto que estén marcadas como 
<q>trans</q>. Es importante que no cambie las líneas que contienen sólo un punto 
en la primera columna. Estos párrafos son separadores y no se verán en los 
sistemas de APT.
</p>

<p>
Los párrafos que ya están traducidos se reutilizan de otras descripciones o de 
una traducción antigua (e incluso indican que el párrafo original en inglés no 
se ha cambiado desde hace tiempo). Cuando cambie un párrafo de ese tipo, no se 
cambiarán todas las descripciones con el mismo párrafo.
</p>

<p>
También tenga en cuenta que cada equipo de traducción tiene sus propias 
preferencias como las listas de palabras o el estilo del entrecomillado. Por 
favor, siga estos estándares tanto como pueda.
# Translators: create a link to your rules from "published"
# Translators: create a link to your language team (mailing list, ...)
 Las reglas más importantes están 
<a href="https://www.debian.org/international/spanish/notas">publicadas</a>. Se 
sugiere que empiece revisando traducciones existentes, ya sea mediante el <a 
href="#DDTSS">DDTSS</a> o navegando en los sistemas de gestión de paquetes como 
<a href="https://packages.debian.org/aptitude">aptitude</a>, para ver las 
preferencias de las traducciones. Si no está seguro contacte con <a 
href="mailto:debian-l10n-spanish@lists.debian.org">el equipo de traducción</a>.
</p>

<toc-add-entry>Revisiones y correcciones de errores</toc-add-entry>
# General proofread suggestions, not DDTSS specific
<p>
Sólo el DDTSS implementa actualmente el modo de revisiones y sólo envía estas 
traducciones al DDTP las que pasan un número fijo de correcciones.
</p>

<p>
Si alguna vez se da cuenta de erratas comunes u otros errores que se pueden 
corregir fácilmente, como problemas con la codificación, es posible saltarse 
cualquier proceso de revisión y corregir a la vez todos los paquetes usando un 
script. Se sugiere que esto sólo lo haga un coordinador de confianza.
</p>

<p>
Ya que las correcciones pueden llevar bastante tiempo (especialmente si sólo son 
correcciones menores), una opción puede ser ignorar las erratas y las 
inconsistencias durante la revisión y comenzar la comprobación de todos estos 
fallos (recolectados) después. Esto aumenta la velocidad de las revisiones y 
permite aplicar estas correcciones en todas las descripciones más tarde.
</p>

<toc-add-entry>Uso de las traducciones</toc-add-entry>
<p>
Se pueden usar la descripciones traducidas de paquetes desde que el paquete de 
APT de <a href="https://packages.debian.org/testing/admin/apt">lenny</a> realiza 
esta tarea. Usando este paquete todos los usuarios pueden tener las 
descripciones en su idioma preferido de todos los programas que usen APT. Esto 
incluye a los programas <tt>apt-cache</tt>, <tt>aptitude</tt>, 
<tt>synaptic</tt>, y algunos otros.
</p>

<p>
# Translators: use a proper mirror!
APT descarga los archivos <tt>Translation-<var>idioma</var></tt> de las réplicas 
de Debian. Estos sólo están disponibles para lenny y las distribuciones 
posteriores.
La ubicación de estos archivos en las réplicas es <a 
href="http://ftp.es.debian.org/debian/dists/sid/main/i18n/">dists/main/sid/i18n/
</a>.
</p>

<p>
También se puede desactivar el uso de las traducciones. Para hacerlo sólo tiene 
que añadir
</p>
<pre>
APT::Acquire::Translation "none";
</pre>
<p>
al archivo <tt>/etc/apt/apt.conf</tt>. En vez de <tt>none</tt> también se puede 
usar el código del idioma.
</p>

<!--
<p>
FIXME: I need to test this script from me again against the new 
Translation-<lang>
files. Ignore this for now:<br />
There is also a small script available which just replaces English descriptions
in the local <tt>Packages</tt> files (<tt>/var/lib/apt/lists/</tt>) with
translations. This could be used if the new APT version cannot be installed for
whatever reasons.
</p>
-->

<toc-add-entry name="todo">Lista de tareas por hacer</toc-add-entry>

<p>
Aunque hay algunos progresos en el DDTP, todavía hay muchas cosas por hacer:
</p>
<ul>
  <li>Todos los equipos de traducción buscan nuevos traductores y revisores que 
puedan ayudar a procesar la enorme lista de paquetes.</li>
  <li>Permitir mejorar las descripciones de paquetes en inglés durante el 
proceso de traducción/revisión. Quizás se podría conseguir añadiendo un nuevo 
pseudoidioma de inglés que contenga las descripciones mejoradas como traducción 
y rellenando automáticamente los fallos tras una revisión.</li>
  <li>El nuevo entorno de internacionalización basado en Pootle necesita mucho 
trabajo hasta que cumpla todas nuestras ideas.</li>
</ul>


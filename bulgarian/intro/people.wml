#use wml::debian::template title="Хората: кои сме ние и с какво се занимаваме" MAINPAGE=true
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="5eef343480ec39f5bb8db5e4e69bbb9e8b926c9b"

# translators: some text is taken from /intro/about.wml

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div id="toc">
  <ul class="toc">
    <li><a href="#history">Как започна всичко</a>
    <li><a href="#devcont">Сътрудници</a>
    <li><a href="#supporters">Хора и организации, които помагат на Дебиан</a>
    <li><a href="#users">Кой използва Дебиан</a>
  </ul>
</div>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Тъй като много хора питат как се произнася Дебиан, отговорът е /&#712;де-би-ан/. Произхожда от имената на създателя на Дебиан, Иан Мърдок и съпругата му Дебра.</p>
</aside>

<h2><a id="history">Как започна всичко</a></h2>

<p>Проектът Дебиан е започнат през август 1993г. от Иан Мърдок (Ian Murdock)
като нова дистрибуция, която да бъде разработвана открито, в духа на Linux и
GNU. Тогава той изпраща покана до всички програмисти с молба да помогнат на
софтуерна дистрибуция, базирана на ядрото Линукс, което по онова време е било
сравнително ново. Основна цел на Дебиан е била внимателният и грижлив подбор на
компоненти и технологии, които да работят добре заедно, базирани на отворен
дизайн и помощ и поддръжка от обществото на свободния софтуер.</p>

<p>Всичко започва от малка, сплотена група от хакери на свободен софтуер,
която прогресивно нараства през годините, за да стане голяма, добре
организирана общност от програмисти и потребители.</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="$(DOC)/manuals/project-history/">Подробна история на Дебиан</a></button></p>

<h2><a id="devcont">Сътрудници</a></h2>

<p>
Дебиан е организация, съставена от доброволци. Повече хиляда активни сътрудници
са пръснати по <a href="$(DEVEL)/developers.loc">целия свят</a> и работят по
Дебиан в свободното си време. Малко от тях са се срещали лице в лице. Основната
комуникацията се осъществява чрез e-mail (например чрез пощенските списъци на
lists.debian.org) и IRC (канала #debian на irc.debian.org).
</p>

<p>
Пълният списък на членовете на Дебиан е достъпен на <a
href="https://nm.debian.org/members">nm.debian.org</a>. Списък на всички
сътрудници и екипи, допринасящи за Дебиан има на <a
href="https://contributors.debian.org">contributors.debian.org</a>.</p>

<p>Проектът Дебиан има добре <a href="organization">организирана структура</a>.
За повече информация относно вътрешната организация разгледайте раздела <a
href="$(DEVEL)/">за разработчици</a>.</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="philosophy">Повече за философията на проекта</a></button></p>

<h2><a id="supporters">Хора и организации, които помагат на Дебиан</a></h2>

<p>Много други хора и организации участват в общността:</p>

<ul>
  <li><a href="https://db.debian.org/machines.cgi">Спонсори на хостинг и хардуер</a></li>
  <li><a href="../mirror/sponsors">Спонсори на огледални сървъри</a></li>
  <li><a href="../partners/">Партньори в разработката и услугите</a></li>
  <li><a href="../consultants">Консултанти</a></li>
  <li><a href="../CD/vendors">Доставчици на инсталационни носители с Дебиан</a></li>
  <li><a href="../distrib/pre-installed">Доставчици на компютри с инсталирана Дебиан</a></li>
  <li><a href="../events/merchandise">Доставчици на сувенири</a></li>
</ul>

<h2><a id="users">Кой използва Дебиан?</a></h2>

<p>
Дебиан се използва от всякакви организации, малки и големи, както и от
многохилядни индивидуални потребители. Прегледайте страницата <a
href="../users/">Кой използва Дебиан?</a> за списък на образователни,
бизнес организации, организации с идеална цел и правителствени агенции, които
са изпратили кратко описание за това как и защо използват Дебиан.
</p>

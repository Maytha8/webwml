#use wml::debian::template title="Spiegel-Sites von Debian (weltweit)" MAINPAGE="true"
#use wml::debian::translation-check translation="0ef84f1326b21d5b0e941b3e5987030eb9f366ac"
# $Id$
# Translator: Helge Kreutzmann <debian@helgefjell.de>, 2007-11-25
# Updated: Holger Wansing <linux@wansing-online.de>, 2016-2017, 2021.
# Updated: Holger Wansing <hwansing@mailbox.org>, 2022.

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
  <li><a href="#per-country">Debian-Spiegel nach Ländern</a></li>
  <li><a href="#complete-list">Vollständige Liste der Spiegel des Debian-Archivs</a></li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Debian wird auf Hunderte von Servern im Internet
         verteilt (<a href="https://www.debian.org/mirror/">gespiegelt</a>). Wenn Sie vorhaben,
         Debian <a href="../download">herunterzuladen</a>, versuchen Sie als erstes einen Server,
         der nahe bei Ihnen liegt. Er wird vermutlich schneller sein als andere, und darüber hinaus
         reduziert dies die Last auf unseren zentralen Archiv-Servern.<p>
</aside>

<p class="centerblock">
  Debian-Spiegel gibt es in vielen Ländern, und für einige davon haben wir
  Alias-Namen nach dem Schema <code>ftp.&lt;länderkürzel&gt;.debian.org</code> eingerichtet.
  Dieser Alias verweist üblicherweise auf einen Spiegel, der regelmäßig und
  schnell synchronisiert wird und alle Debian-Architekturen enthält. Das
  Debian-Archiv ist über HTTP auf dem Server immer unter <code>/debian</code>
  zu finden.
</p>

<p class="centerblock">
  Andere Spiegel-Server könnten (aufgrund von Platzbeschränkungen)
  gewissen Einschränkungen unterliegen, was gespiegelt wird. Lediglich die Tatsache,
  dass ein Server nicht der <code>ftp.&lt;länderkürzel&gt;.debian.org</code>-Spiegel
  eines Landes ist, bedeutet nicht, dass ein solcher langsamer oder
  weniger aktuell ist als der <code>ftp.&lt;länderkürzel&gt;.debian.org</code>-Spiegel.
  Im Gegenteil: ein Spiegel, der die von Ihnen benötigte Architektur enthält und
  in Ihrer Nähe liegt (und aufgrunddessen für Sie schneller ist), ist anderen
  Spiegeln, die weiter entfernt liegen, fast immer vorzuziehen.
</p>

<p>Verwenden Sie den Server, der Ihnen am nächsten liegt, um die höchste
   Geschwindigkeit zu erreichen &ndash; egal ob es der Alias-Spiegel Ihres Landes ist
   oder nicht. Das
   Programm <a href="https://packages.debian.org/stable/net/netselect-apt">\
   <code>netselect-apt</code></a> kann zur Bestimmung der Site mit der geringsten
   Latenzzeit bestimmt werden; verwenden Sie Programme zum Herunterladen wie
   <a href="https://packages.debian.org/stable/web/wget"><code>wget</code></a> oder
   <a href="https://packages.debian.org/stable/net/rsync"><code>rsync</code></a> zur
   Bestimmung der Site mit dem größten Durchsatz. Beachten Sie, dass 
   geografische Nähe oft nicht der wichtigste Faktor bei der Wahl des für Sie am
   besten geeigneten Servers ist.</p>

<p>
Wenn Sie mit Ihrem Rechner viel unterwegs sind, sind Sie mit einem
<q>Spiegel</q>, der ein globales <abbr title="Content Delivery Network">CDN</abbr>
im Rücken hat, vielleicht am besten bedient. Das Debian-Projekt betreibt
für diesen Zweck <code>deb.debian.org</code> und Sie können diesen Dienst auch in
Ihrer <code>sources.list</code> eintragen &ndash; besuchen Sie dessen
<a href="http://deb.debian.org/">Website</a> bezüglich weiterer Details.
</p>

<h2 id="per-country">Debian-Spiegel nach Ländern</h2>

<table border="0" class="center">
<tr>
  <th>Land</th>
  <th>Site</th>
  <th>Architekturen</th>
</tr>
#include "$(ENGLISHDIR)/mirror/list-primary.inc"
</table>

<h2 id="complete-list">Vollständige Liste der Spiegel des Debian-Archivs</h2>

<table border="0" class="center">
<tr>
  <th>Rechnername</th>
  <th>HTTP</th>
  <th>Architekturen</th>
</tr>
#include "$(ENGLISHDIR)/mirror/list-secondary.inc"
</table>

#include "$(ENGLISHDIR)/mirror/list-footer.inc"

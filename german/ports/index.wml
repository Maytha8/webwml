#use wml::debian::template title="Portierungen"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::toc
#use wml::debian::translation-check translation="e0652549b916d3585e3eed9d093a6711af721019"
# $Id$
# Translation: Dirk Niemeyer <dirk.niemeyer@num.de>
# Updated: Holger Wansing <linux@wansing-online.de>, 2011, 2014 - 2018.
# Updated: Holger Wansing <hwansing@mailbox.org>, 2019, 2020, 2023.

<toc-display/>

<toc-add-entry name="intro">Einleitung</toc-add-entry>
<p>
Wie viele von Ihnen wissen, ist <a href="https://www.kernel.org/">Linux</a> nur
ein Betriebssystemkern. Und für eine lange Zeit lief der
Linux-Betriebssystemkern nur auf Rechnern der Intel x86-Serie, beginnend beim 386.
</p>
<p>
Allerdings ist dies nun nicht mehr so. Der Linux-Betriebssystemkern ist auf
eine große, weiter wachsende Zahl von Architekturen portiert worden.
Und diesem folgend haben wir auch unsere Debian-Distribution auf weitere Architekturen
portiert. Meist ist dies ein Prozess mit einem langsamen Start (während
wir libc und den dynamischen Linker dazu bringen, reibungslos zu arbeiten), und
dann relativer Routine, wenn nicht sogar Langeweile, während wir
versuchen, alle Pakete für die neue Architektur zu übersetzen.
</p>
<p>
Debian ist ein Betriebssystem, kein Betriebssystemkern.
(Eigentlich ist es inzwischen mit seinen tausenden enthaltenen Programmen
sogar mehr als nur ein Betriebssystem.)
Dementsprechend basieren die meisten Debian-Portierungen auf Linux, jedoch
gibt es auch Portierungen basierend auf den FreeBSD-, NetBSD- und Hurd-Kernels.
</p>

<div class="important">
<p>
Diese Seite ist in der Entwicklung. Nicht alle
Portierungen haben
schon eigene Seiten und die meisten finden Sie unter externen Web-Adressen. Wir
arbeiten daran, Informationen über alle Ports zu sammeln, um diese dann mit
der Debian-Website zu synchronisieren. Im
<a href="https://wiki.debian.org/CategoryPorts">Wiki</a> sind eventuell weitere
Portierungen aufgelistet.
</p>
</div>

<toc-add-entry name="portlist-released">Liste offizieller Portierungen</toc-add-entry>

<p>
Diese Portierungen sind die offiziell vom Debian-Projekt unterstützten Architekturen
und entweder Teil einer offiziellen Veröffentlichung oder als Bestandteil der nächsten
Veröffentlichung vorgesehen.
</p>

<table class="tabular" summary="">
<tbody>
<tr>
<th>Portierung</th>
<th>Architektur</th>
<th>Beschreibung</th>
<th>Hinzugefügt</th>
<th>Status</th>
</tr>
<tr>
<td><a href="amd64/">amd64</a></td>
<td>64-Bit-PC (amd64)</td>
<td>Portierung auf 64-Bit-Prozessoren mit dem Ziel, sowohl 32-Bit- als auch
    64-Bit-Benutzeranwendungen zu unterstützen. Diese Portierung unterstützt AMDs 64-Bit Opteron-, 
    Athlon- und Sempron-Prozessoren sowie Intels Prozessoren mit Intel 64-Unterstützung,
    darunter den Pentium&nbsp;D und verschiedene Xeon- und Core-Serien.
</td>
<td>4.0</td>
<td><a href="$(HOME)/releases/stable/amd64/release-notes/">veröffentlicht</a></td>
</tr>
<tr>
<td><a href="arm/">arm64</a></td>
<td>64-Bit-ARM (AArch64)</td>
<td>Portierung auf die 64-Bit ARM-Architektur mit der neuen Version 8 des 64-Bit-Befehlssatzes
    (genannt AArch64); für Prozessoren wie den Applied Micro X-Gene, den AMD Seattle
    und den Cavium ThunderX.
</td>
<td>8</td>
<td><a href="$(HOME)/releases/stable/arm64/release-notes/">veröffentlicht</a></td>
</tr>
<tr>
<td><a href="arm/">armel</a></td>
<td>EABI ARM</td>
<td>Portierung auf die 32-Bit Little-Endian ARM-Architektur; nutzt die Embedded ABI,
    die ARM-CPUs unterstützt, welche mit dem v5te-Befehlssatz kompatibel sind. 
    Diese Portierung nutzt nicht die Vorteile von Fließkomma-Einheiten (FPU).
</td>
<td>5.0</td>
<td><a href="$(HOME)/releases/stable/armel/release-notes/">veröffentlicht</a></td>
</tr>
<tr>
<td><a href="arm/">armhf</a></td>
<td>Hard Float ABI ARM</td>
<td>Portierung auf die 32-Bit Little-Endian ARM-Architektur für Boards und Geräte,
    die eine Fließkomma-Einheit (FPU) enthalten sowie andere moderne CPU-Funktionalitäten.
    Diese Portierung erfordert mindestens eine ARMv7-CPU mit Thumb-2 und
    VFPv3-D16 Fließkomma-Unterstützung.
</td>
<td>7.0</td>
<td><a href="$(HOME)/releases/stable/armhf/release-notes/">veröffentlicht</a></td>
</tr>
<tr>
<td><a href="i386/">i386</a></td>
<td>32-Bit-PC (i386)</td>
<td>Portierung auf 32-Bit x86-Prozessoren, auf der Linux ursprünglich entwickelt
    wurde, daher der Kurzname.
    Debian unterstützt alle IA-32 Prozessoren, hergestellt von Intel (darunter alle
    Pentium-Serien und die neuesten Core Duo-Maschinen im 32-Bit-Modus), AMD (K6, 
    alle Athlon-Serien, Athlon64-Serien im 32-Bit-Modus), Cyrix
    und weiteren Herstellern.
</td>
<td>1.1</td>
<td><a href="$(HOME)/releases/stable/i386/release-notes/">veröffentlicht</a></td>
</tr>
<tr>
<td><a href="https://wiki.debian.org/mips64el">mips64el</a></td>
<td>MIPS (64-Bit Little-Endian-Mode)</td>
<td>Portierung auf die Little-Endian N64-ABI für MIPS64r1 ISA und
    Hardware-Fließkomma-Einheit.
</td>
<td>9</td>
<td><a href="$(HOME)/releases/stable/mips64el/release-notes/">veröffentlicht</a></td>
</tr>
<tr>
<td><a href="powerpc/">ppc64el</a></td>
<td>POWER7+, POWER8</td>
<td>Portierung auf die 64-Bit Little-Endian POWER-Architektur;
    nutzt die neue OpenPower-ELFv2-ABI.</td>
<td>8</td>
<td><a href="$(HOME)/releases/stable/ppc64el/release-notes/">veröffentlicht</a></td>
</tr>
<tr>
<td><a href="https://wiki.debian.org/RISC-V">riscv64</a></td>
<td>RISC-V (64-Bit Little-Endian)</td>
<td>Portierung für 64-Bit Little-Endian <a href="https://riscv.org/">RISC-V</a>,
    eine freie/offene ISA.</td>
<td>13</td>
<td>Im Test</td>
</tr>
<tr>
<td><a href="s390x/">s390x</a></td>
<td>System z</td>
<td>Portierung auf die 64-Bit Userland-Umgebung für IBM System-z Mainframes.</td>
<td>7.0</td>
<td><a href="$(HOME)/releases/stable/s390x/release-notes/">veröffentlicht</a></td>
</tr>
</tbody>
</table>

<toc-add-entry name="portlist-unreleased">Liste anderer Portierungen</toc-add-entry>

<p>
Diese Portierungen sind entweder im Aufbau mit der Absicht, eventuell in
offiziell unterstützte Architekturen überführt zu werden, waren in der
Vergangenheit einmal offiziell unterstützt, sind aber mittlerweile nicht mehr
Teil der Veröffentlichung, da sie die dazu erforderliche Qualifikation nicht
erfüllten oder wegen fehlendem ausreichenden Interesse seitens der Entwickler,
oder es wird nicht mehr daran gearbeitet und sie sind nur aus historischem
Interesse noch hier aufgeführt.
</p>

<p>
Portierungen aus diesem Bereich - falls noch aktiv betreut - sind über die
<url "https://www.ports.debian.org/">-Infrastruktur verfügbar.
</p>

<div class="tip">
<p>
 Es gibt inoffizielle Installations-Images für einige der folgenden Portierungen unter
 <a href="https://cdimage.debian.org/cdimage/ports">https://cdimage.debian.org/cdimage/ports</a>.
 Diese Images werden nur von den jeweiligen Debian-Ports-Teams betreut.
</p>
</div>


<table class="tabular" summary="">
<tbody>
<tr>
<th>Portierung</th>
<th>Architektur</th>
<th>Beschreibung</th>
<th>Hinzugefügt</th>
<th>Entfernt</th>
<th>Status</th>
<th>Abgelöst durch</th>
</tr>
<tr>
<td><a href="alpha/">alpha</a></td>
<td>Alpha</td>
<td>Portierung auf die 64-Bit RISC-Alpha-Architektur.
</td>
<td>2.1</td>
<td>6.0</td>
<td>ports</td>
<td>-</td>
</tr>
<tr>
<td><a href="arm/">arm</a></td>
<td>OABI ARM</td>
<td>Portierung auf die ARM-Architektur unter Nutzung der alten ABI.
</td>
<td>2.2</td>
<td>6.0</td>
<td>tot</td>
<td>armel</td>
</tr>
<tr>
<td><a href="https://web.archive.org/web/20130326061253/http://avr32.debian.net/">avr32</a></td>
<td>Atmel 32-Bit RISC</td>
<td>Portierung auf Atmels 32-Bit RISC-Architektur AVR32.
</td>
<td>-</td>
<td>-</td>
<td>tot</td>
<td>-</td>
</tr>
<tr>
<td><a href="hppa/">hppa</a></td>
<td>HP PA-RISC</td>
<td>Portierung auf Hewlett-Packard's PA-RISC-Architektur.
</td>
<td>3.0</td>
<td>6.0</td>
<td>ports</td>
<td>-</td>
</tr>
<tr>
<td><a href="hurd/">hurd-i386</a></td>
<td>32-Bit-PC (i386)</td>
<td>Portierung auf das GNU-Hurd-Betriebssystem, für 32-Bit x86-Prozessoren.
</td>
<td>-</td>
<td>-</td>
<td>ports</td>
<td>-</td>
</tr>
<tr>
<td><a href="hurd/">hurd-amd64</a></td>
<td>64-Bit-PC (amd64)</td>
<td>Portierung auf das GNU-Hurd-Betriebssystem, für 64-Bit x86-Prozessoren.
    Sie unterstützt nur 64-Bit, nicht 32-Bit via 64-Bit.
</td>
<td>-</td>
<td>-</td>
<td>ports</td>
<td>-</td>
</tr>
<tr>
<td><a href="ia64/">ia64</a></td>
<td>Intel Itanium IA-64</td>
<td>Portierung auf Intels erste 64-Bit-Architektur.
    Beachten Sie: diese Portierung sollte <em>nicht</em> mit der jüngsten 
    64-Bit-Erweiterung von Intel für Pentium 4 und Celeron-Prozessoren mit
    dem Namen Intel 64 verwechselt werden; diese wird durch die
    AMD64-Portierung abgedeckt.
</td>
<td>3.0</td>
<td>8</td>
<td>ports</td>
<td>-</td>
</tr>
<tr>
<td><a href="kfreebsd-gnu/">kfreebsd-amd64</a></td>
<td>64-Bit-PC (amd64)</td>
<td>Portierung auf den Kernel von FreeBSD unter Verwendung der glibc.
    Sie wurde als erste Nicht-Linux-Portierung von Debian als
    Technologie-Vorschau veröffentlicht.
</td>
<td>6.0</td>
<td>8</td>
<td><a href="https://lists.debian.org/debian-devel/2023/05/msg00306.html">tot</a></td>
<td>-</td>
</tr>
<tr>
<td><a href="kfreebsd-gnu/">kfreebsd-i386</a></td>
<td>32-Bit-PC (i386)</td>
<td>Portierung auf den Kernel von FreeBSD unter Verwendung der glibc.
    Sie wurde als erste Nicht-Linux-Portierung von Debian als
    Technologie-Vorschau veröffentlicht.
</td>
<td>6.0</td>
<td>8</td>
<td><a href="https://lists.debian.org/debian-devel/2023/05/msg00306.html">tot</a></td>
<td>-</td>
</tr>
<tr>
<td><a href="https://wiki.debian.org/LoongArch">loong64</a></td>
<td>LoongArch (64-Bit Little-Endian)</td>
<td>Portierung für die 64-Bit Little-Endian LoongArch-Architektur.</td>
<td>-</td>
<td>-</td>
<td>ports</td>
<td>-</td>
<tr>
<td><a href="http://www.linux-m32r.org/">m32</a></td>
<td>M32R</td>
<td>Portierung auf die 32-Bit <acronym lang="en" 
    title="Reduced Instruction Set Computer">RISC</acronym>-Mikroprozessoren
    von Renesas Technology.
</td>
<td>-</td>
<td>-</td>
<td>tot</td>
<td>-</td>
</tr>
<tr>
<td><a href="m68k/">m68k</a></td>
<td>Motorola 68k</td>
<td>Portierung auf die Motorola 68k-Prozessorserien &ndash; insbesondere
    die Sun3-Workstation-Familie, die Apple Macintosh Personal-Computer sowie
    die Atari und Amiga Personal-Computer.
</td>
<td>2.0</td>
<td>4.0</td>
<td>ports</td>
<td>-</td>
</tr>
<tr>
<td><a href="mips/">mips</a></td>
<td>MIPS (Big-Endian-Modus)</td>
<td>Portierung auf die MIPS-Architektur, die in (Big-Endian) SGI-Maschinen verwendet wird.
</td>
<td>3.0</td>
<td>11</td>
<td><a href="https://lists.debian.org/debian-release/2019/08/msg00582.html">tot</a></td>
<td>-</td>
</tr>
<tr>
<td><a href="mips/">mipsel</a></td>
<td>MIPS (Little-Endian-Modus)</td>
<td>Portierung auf die MIPS-Architektur, die in (Little-Endian) Digital DECstations verwendet wird.
</td>
<td>3.0</td>
<td>13</td>
<td><a href="https://lists.debian.org/debian-devel-announce/2023/09/msg00000.html">dead</a></td>
<td>-</td>
</tr>
<tr>
<td><a href="https://web.archive.org/web/20230605050614/http://www.debian.org/ports/netbsd/">netbsd-i386</a></td>
<td>32-Bit-PC (i386)</td>
<td>Portierung auf den NetBSD-Kernel mit libc, für 32-Bit x86-Prozessoren.
</td>
<td>-</td>
<td>-</td>
<td>tot</td>
<td>-</td>
</tr>
<tr>
<td><a href="https://web.archive.org/web/20230605050614/http://www.debian.org/ports/netbsd/">netbsd-alpha</a></td>
<td>Alpha</td>
<td>Portierung auf den NetBSD-Kernel mit libc, für 64-Bit Alpha-Prozessoren.
</td>
<td>-</td>
<td>-</td>
<td>tot</td>
<td>-</td>
</tr>
<tr>
<td><a href="https://web.archive.org/web/20150905061423/http://or1k.debian.net/">or1k</a></td>
<td>OpenRISC 1200</td>
<td>Portierung auf die <a href="https://openrisc.io/">OpenRISC</a> 1200 Open-Source-CPU.
</td>
<td>-</td>
<td>-</td>
<td>tot</td>
<td>-</td>
</tr>
<tr>
<td><a href="powerpc/">powerpc</a></td>
<td>Motorola/IBM PowerPC</td>
<td>Portierung auf viele Apple Macintosh PowerMac-Modelle sowie Rechner der
    offenen CHRP- und PReP-Architekturen.
</td>
<td>2.2</td>
<td>9</td>
<td>ports</td>
<td>-</td>
</tr>
<tr>
<td><a href="https://wiki.debian.org/PowerPCSPEPort">powerpcspe</a></td>
<td>PowerPC Signal-Processing-Engine</td>
<td>Portierung auf die <q>Signal-Processing-Engine</q>-Hardware,
    vorhanden auf den energiesparenden 32-Bit FreeScale- und IBM <q>e500</q>-CPUs.
</td>
<td>-</td>
<td>-</td>
<td>tot</td>
<td>-</td>
</tr>
<tr>
<td><a href="s390/">s390</a></td>
<td>S/390 und zSeries</td>
<td>Portierung auf IBM S/390 Server.
</td>
<td>3.0</td>
<td>8</td>
<td>tot</td>
<td>s390x</td>
<tr>
<td><a href="sparc/">sparc</a></td>
<td>Sun SPARC</td>
<td>Portierung auf Workstations der UltraSPARC-Familie sowie einen Teil von
    deren Nachfolgern in den Sun4-Architekturen.
</td>
<td>2.1</td>
<td>8</td>
<td>tot</td>
<td>sparc64</td>
</tr>
<tr>
<td><a href="https://wiki.debian.org/Sparc64">sparc64</a></td>
<td>64-Bit SPARC</td>
<td>Portierung auf 64-Bit SPARC-Prozessoren.
</td>
<td>-</td>
<td>-</td>
<td>ports</td>
<td>-</td>
</tr>
<tr>
<td><a href="https://wiki.debian.org/SH4">sh4</a></td>
<td>SuperH</td>
<td>
Portierung auf Hitachis SuperH-Prozessoren. Unterstützt auch den
Open-Source-Prozessor <a href="https://j-core.org/">J-Core</a>.
</td>
<td>-</td>
<td>-</td>
<td>ports</td>
<td>-</td>
</tr>
<tr>
<td><a href="https://wiki.debian.org/X32Port">x32</a></td>
<td>64-Bit-PC mit 32-Bit-Zeigern</td>
<td>Portierung auf die amd64/x86_64 x32-ABI, die den amd64-Befehlssatz verwendet,
    aber mit 32-Bit-Zeigern (um den größeren Bereich verfügbarer Register von x64_64
    mit dem kleineren Speicher- und Cache-Bedarf zu kombinieren, der aus der Nutzung
    von 32-Bit-Zeigern resultiert).
</td>
<td>-</td>
<td>-</td>
<td>ports</td>
<td>-</td>
</tr>
</tbody>
</table>


<div class="warning">
<p>
Viele der obigen Computer- und
Prozessornamen sind Warenzeichen oder eingetragene Warenzeichen ihrer
jeweiligen Hersteller. Sie werden ohne Erlaubnis benutzt.
</p>
</div>
